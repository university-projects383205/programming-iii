#!/bin/bash

set -o posix

if [[ $1 ]] && [[ $1 =~ ^[0-9]+$ ]] && [[ $1 -gt -1 ]]; then
  if [[ $(command -v gnome-terminal) ]] && [[ $(command -v ant) ]]; then
    if [[ $2 ]] && [[ $2 == "ns" ]]; then
      spawn_server=0
    else
      spawn_server=1
    fi

    names=("tom" "peter" "josh")
    suffix="@test.test"

    for name in ${names[*]}; do
      user="$name$suffix"
      mkdir -p "users/$user"
    done

    if [[ $PATH_TO_FX ]]; then
      sed -i "s;^path_to_fx = .*;path_to_fx = $PATH_TO_FX;" build.properties
    fi

    ant build-all

    if [[ $spawn_server -eq 1 ]]; then
      gnome-terminal -q --tab --title="Server" -- ant run-server
    fi

    if [[ $1 -gt 0 ]]; then
      for _ in $(seq 1 "$1"); do
        gnome-terminal -q --tab --title="Client" -- ant run-client
      done
    fi
  else
    echo "[*] Install \"gnome-terminal\" and/or \"ant\" first!"
  fi
else
  echo "Usage:"
  echo "$ ./MailService.sh [clients to spawn]"
fi
