package client;

import common.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

// Invia le operazioni richieste e riceve notifiche e/o mail richieste
public class ChannelClient {
  private UserAgentModel model;
  private LoginController loginController;
  private ArrayList<ComposerController> composerControllers;
  private StringProperty myAddress;
  private Socket socket;
  private ObjectOutputStream out;
  private ObjectInputStream in;
  private BooleanProperty status;  // Per aggiornare l'icona di stato

  public ChannelClient(){
    myAddress = new SimpleStringProperty();
    status = new SimpleBooleanProperty(false);
  }

  // Indirizzo casella
  public StringProperty getMyAddress(){
    return myAddress;
  }

  public void setModel(UserAgentModel m){
    loginController = null;
    composerControllers = new ArrayList<>(3);
    model = m;
  }

  public void setLogin(LoginController l){
    loginController = l;
  }

  public void setMyAddress(String myAddress) {
    loginController = null;
    this.myAddress.set(myAddress);
  }

  public void addComposerController(ComposerController cc){
    composerControllers.add(cc);
  }
  public void removeComposerController(ComposerController cc){
    composerControllers.remove(cc);
  }
  public int sizeComposerControllers(){
    return composerControllers.size();
  }
  public void closeAllComposerControllers(){
    for(int i = 0; i < composerControllers.size(); i++){
      composerControllers.get(i).close();
    }
  }

  // Quando ricevi un messaggio devi sapere a quale composer inviarlo (e.g. per sapere chi notificare)
  public void composerMsg(Operation op){
    int id = op.getComposerID();
    boolean found = false;
    // System.out.println("a chi lo dico?");
    for(int i = 0; !found && i < composerControllers.size(); i++){
      // System.out.println("composers id: " + composerControllers.get(i).getId() + ", a chi arriva: " + id);

      found = composerControllers.get(i).getId() == id;
      if(found){
        // System.out.println("trovato a chi dirlo");
        String msg = op.getComposerMsg();
        String[] msgs = msg.split("&");
        if(msgs.length == 2){
          for(int j = 0; j < msgs.length; j++){
            if(msgs[j].equals(Operation.FLAG_SUCCESS)){
              model.newMail("Sent");
            }else{
              msg = msgs[j];
            }
          }
        }else{
          System.err.println("[ChannelClient][composerMsg]: Ricevuti più messaggi. Errore");
        }
        composerControllers.get(i).setMsg(msg);
        if(msg.equals(Operation.FLAG_SUCCESS)){
          model.newMail("Sent");
        }
      }//end found
    }//end for
  }

  public void errorMsg(Operation op){
    System.err.println("E' arrivato un error msg... AAAAAAAAAAAAH");
  }

  // Apre la connessione con il server
  public boolean open(){
    boolean output = true;
    try{
        socket = new Socket("127.0.0.1", 4862);
        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());
        receive();

    }catch(EOFException | SocketException e) {
      System.err.println("[ChannelClient]: Server is offline (" + e.getMessage() + ")");
      in = null;
      out = null;
      status.set(false);
      if(loginController != null){
        // Il controller del login esiste finché il server è offline e/o quando il client si connette con successo
        loginController.responseLogin(null, "Server is offline. Retry later");
      }
    }catch (IOException ex) {
      ex.printStackTrace();
      output = false;
    }
    return output;
  }

  public void close(){
    if(socket != null){
      try{
        socket.close();
      }catch (IOException ex){
        ex.printStackTrace();
      }
    }
  }

  public void disconnected(){
    status.set(false);
  }

  public void send(Object operation){
    SenderClient rs = new SenderClient(this, out, operation);
    rs.start();
  }

  public void receive(){
    ReceiverClient rc = new ReceiverClient(this, in);
    rc.start();
  }

  // Notifica una nuova mail
  public void newMail(Operation op){
    String str = op.getNotify();
    model.newMail(str);
  }

  public void loadMails(Operation op){
    int id = op.getId();
    ArrayList<Mail> mails = op.getMails();
    model.loadMails(id, mails);
  }

  // Check sulla validità dell'utente
  public void responseLogin(Operation op){
    String addr = op.getMyAddress();
    String msg = op.getLoginMsg();
    loginController.responseLogin(addr, msg);
    if(msg.equals(Operation.FLAG_SUCCESS)){
      status.set(true);
    }
  }

  public void logout(){
    myAddress.set("");
    model.logout();
  }

  public BooleanProperty getStatusProperty(){
    return status;
  }

  public boolean getStatus(){
    return status.get();
  }
}
