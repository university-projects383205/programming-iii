package client;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

// Mantiene il conto delle email non lette
public class CounterMails {
  private String folder;
  private IntegerProperty count;

  public CounterMails(String f){
    folder = f;
    count = new SimpleIntegerProperty();
    count.set(0);
  }

  public String getFolder(){
    return folder;
  }

  public void incr(){
    int i = count.getValue();
    count.set(++i);
  }

  public void reset(){
    count.set(0);
  }

  public void set(int c){
    count.set(c);
  }

  public int getCount(){
    return count.get();
  }

  public IntegerProperty getIntegerProperty(){
    return count;
  }

  @Override
  public String toString() {
    return folder + ": " + count.get();
  }
}
