package client;

import common.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;


public class LoginController {

  @FXML
  private Button send_form;

  @FXML
  private TextField email_txt;

  @FXML
  private Label error_msg;

  private ChannelClient channel;

  public LoginController(ChannelClient c){
    channel = c;
    channel.setLogin(this);
  }

  @FXML
  private void enterAction(KeyEvent event) {
    if(event.getCode().equals(KeyCode.ENTER))
      login();
  }

  @FXML
  private void formAction(ActionEvent event) {
    login();
  }

  // Invia informazioni di login
  private void login(){
    channel.open();
    Operation op = new Operation(-1, Operation.LOGIN);
    op.setLogin(email_txt.getText(), null);
    channel.send(op);
  }

  // Attende e verifica l'esito del login
  public void responseLogin(String recMail, String recMsg){
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        if (recMsg.equals(Operation.FLAG_SUCCESS)) {
          System.out.println("[LoginController] Login successfull");
          channel.setMyAddress(recMail);
          Stage stage = (Stage) send_form.getScene().getWindow();
          stage.close();
        } else {
          error_msg.setText(recMsg);
          email_txt.setText("");
          channel.close();
        }//end if
      }//end run
    });
  }//end method
}//end class
