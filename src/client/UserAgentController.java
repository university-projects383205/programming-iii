package client;

import common.Mail;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.*;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class UserAgentController implements Initializable {
  private String[] folders_arr; // Lista delle folder
  private UserAgentModel ua;
  private Stage root_stage;
  private String current_folder;

  @FXML
  private AnchorPane pane;

  @FXML
  private TreeView<String> folders;
  @FXML
  private TableView<Mail> table;

  @FXML
  private TableColumn<Mail, String> subject_col;
  @FXML
  private TableColumn<Mail, String> from_col;
  @FXML
  private TableColumn<Mail, String> date_col;

  @FXML
  private Button refresh_btn;
  @FXML
  private Button compose_btn;
  @FXML
  private Button read_btn;
  @FXML
  private Button unread_btn;
  @FXML
  private Button spam_btn;
  @FXML
  private Button delete_btn;
  @FXML
  private MenuButton menu_move_to;
  @FXML
  private Button empty_btn;
  @FXML
  private Button recover_btn;

  @FXML
  private Label connectionStatus;
  @FXML
  private ProgressBar progressBar;
  @FXML
  private Label connectingLabel;

  @FXML
  private BorderPane showMail;
  @FXML
  private Button reply_btn;
  @FXML
  private Button recompose_btn;
  @FXML
  private Button forward_btn;
  @FXML
  private Button replyall_btn;
  @FXML
  private TextField from_area;
  @FXML
  private TextField to_area;
  @FXML
  private TextField cc_area;
  @FXML
  private TextField date_area;
  @FXML
  private TextField subject_area;
  @FXML
  private TextArea body_area;

  public UserAgentController(UserAgentModel ua, Stage root_stage) {
    this.ua = ua;
    this.folders_arr = ua.getFolders();
    this.root_stage = root_stage;
    this.current_folder = null;

    // Ricevo mail richieste
    this.ua.getNumMails().addListener(
      new ChangeListener<Number>() {
        @Override
        public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
          Platform.runLater(new Runnable() {
            // Verifica cambiamento numero di mail nella cartella selezionata
            @Override
            public void run() {
              System.out.println("[Controller] size: " + t1.intValue());
              if (t1.intValue() == -1) {
                table.setPlaceholder(new Label("Loading"));
              } else if (t1.intValue() == 0) {
                table.setPlaceholder(new Label("Empty folder"));
              } else {
                ObservableList<Mail> mails = ua.getMails();
                table.setItems(mails);
              } //end if
            }
          }); //runnable
        }
      });//listener

    // Effettuato login con successo
    this.ua.getAddr().addListener(new ChangeListener<String>() {
      @Override
      public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
        Platform.runLater(new Runnable() {
          // Toggle dell'effetto gaussian blur
          @Override
          public void run() {
            folders.getRoot().setValue(t1);
            showMails(current_folder);
            pane.setEffect(null);
            pane.setDisable(false);
          }
        });
      }
    });

    // Effettuato logout e chiudi tutto
    this.ua.getLogout().addListener(new ChangeListener<Boolean>() {
      @Override
      public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            System.out.println("[Controller] logout forzato, rifare login");
            ua.getChannel().closeAllComposerControllers();
            login();
          }
        });
      }
    });

    // Aggiorna la grafica del contatore delle notifiche
    this.ua.getCounterNewMail().addListener(new ListChangeListener<CounterMails>() {
      @Override
      public void onChanged(Change<? extends CounterMails> change) {
        List<? extends CounterMails> list = change.getList();

        for(int i = 0; i < folders_arr.length; i++){
          TreeItem<String> trees = folders.getTreeItem(0).getChildren().get(i);
          //System.out.println("i: " + i + ", " + trees);
          String str = "";

          int val = list.get(i).getCount();
          if(val > 0){
            str = val + " ";
          }
          trees.setValue(str + folders_arr[i]);
        }
      }
    });

    // Listenere sul cambiamento di stato del client
    ua.getChannel().getStatusProperty().addListener(new ChangeListener<Boolean>() {
      @Override
      public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean t1) {
        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            if(t1){
              setStatusBar("connected");
            }else{
              setStatusBar("disconnected");
            }
          }
        });
      }
    });
  }

  // Login
  public void login(){
    try {
      ColorAdjust ca = new ColorAdjust(0, -0.9, -0.5, 0);
      GaussianBlur gb = new GaussianBlur(3); // Gaussian blur <===> 30L
      ca.setInput(gb); // Imposto il gaussian blur
      pane.setEffect(ca);

      FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));
      LoginController loginController = new LoginController(ua.getChannel());
      loader.setController(loginController);
      Parent root = loader.load();

      Scene scene = new Scene(root);
      Stage stage = new Stage();

      // Chiudi tutto quando chiudi la finestra di login
      stage.setOnCloseRequest(event -> {
        System.out.println("[Controller] Chiuse le finestre da stage login");
        ua.close();
        Platform.exit();
      });
      stage.setTitle("Login");
      stage.setScene(scene);

      int loginWidth = 300;
      int loginHeight = 400;
      setLoginWin(stage, loginWidth, loginHeight);
      // Imposta le dimensioni della finestra
      root_stage.widthProperty().addListener((obs, oldVal, newVal) -> {
        setLoginWin(stage, loginWidth, loginHeight);
      });
      root_stage.heightProperty().addListener((obs, oldVal, newVal) -> {
        setLoginWin(stage, loginWidth, loginHeight);
      });
      root_stage.xProperty().addListener((obs, old, nie) -> {
        setLoginWin(stage, loginWidth, loginHeight);
      });
      root_stage.yProperty().addListener((obs, old, nie) -> {
        setLoginWin(stage, loginWidth, loginHeight);
      });
      // Chiudi tutto quando chiudi la finestra principale
      root_stage.setOnCloseRequest(event -> {
        System.out.println("[Controller] Chiuse le finestre da stage principale");
        ua.close();
        Platform.exit();
      });
      root_stage.setMinWidth(loginWidth + 100);
      root_stage.setMinHeight(loginHeight + 100);
      pane.setDisable(true);

      stage.initOwner(root_stage);
//      stage.initModality(Modality.WINDOW_MODAL);
      stage.setResizable(false);
      stage.initStyle(StageStyle.UNDECORATED); // Set stage init style to StageStyle.UNDECORATED
      stage.show();

    }catch (Exception e){
      e.printStackTrace();
    }
  }

  private void setLoginWin(Stage stage, int loginWidth, int loginHeight){
    double x = root_stage.getX() + (root_stage.getWidth() / 2 - loginWidth / 2);
    double y = root_stage.getY() + (root_stage.getHeight() / 2 - loginHeight / 2);
    stage.setX(x);
    stage.setY(y);
    //System.out.println("[Controller] x: " + x + ", y: " + y);
  }

  public void tableRefresh(){
    this.table.refresh();
  }

  public String getCurrent_folder(){
    return current_folder;
  }

  @FXML
  private void refreshButtonAction(ActionEvent event) {
    System.out.println("[*] REFRESH");

    this.table.getItems().clear();
    this.ua.getMails(this.current_folder);
  }

  @FXML
  private void composeButtonAction(ActionEvent event) {
    System.out.println("[*] COMPOSE");
    composerWindow(null); // Crei da zero una nuova mail
  }

  @FXML
  private void recomposeButtonAction(ActionEvent event) {
    System.out.println("[*] RECOMPOSE");
    Mail mail = this.table.getSelectionModel().getSelectedItem();
    Mail to_recompose = new Mail(null, mail.getTo(), mail.getDate(), mail.getSubject(), mail.getBody(), mail.getCc(), false, false);
    composerWindow(to_recompose); // Riapre una mail in drafts dal punto in cui era rimasto
  }

  @FXML
  private void forwardButtonAction(ActionEvent event) {
    Mail mail = this.table.getSelectionModel().getSelectedItem();
    Mail to_forward = new Mail(null, "", new Date().getTime(), "[FWD]: " + mail.getSubject(), mail.getBody(), null, false, false);
    composerWindow(to_forward);
  }

  @FXML
  private void replyallButtonAction(ActionEvent event) {
    Mail mail = this.table.getSelectionModel().getSelectedItem();
    String to = current_folder.equals("sent") ? mail.getTo() : mail.getFrom();
    Mail correctMail = new Mail(null, to,  new Date().getTime(), "[RE]: " + mail.getSubject(), "", mail.getCc(), false, false);
    composerWindow(correctMail);
  }

  private static int idCounter = 0;
  private void composerWindow(Mail mail){
    if(!ua.getChannel().getStatus()){
      ua.getMails("disconnection");
    } else if(ua.getChannel().sizeComposerControllers() < 3){
      try {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("composer.fxml"));
        ComposerController composer_controller = new ComposerController(idCounter++, ua, this, mail);
        ua.getChannel().addComposerController(composer_controller);
        loader.setController(composer_controller);
        Parent root = loader.load();
        Scene composer_scene = new Scene(root, 488, 575);
        composer_scene.getStylesheets().add(getClass().getResource("composer.css").toExternalForm());

        Platform.runLater(new Runnable() {
          @Override
          public void run() {
            Stage composer = new Stage();
            composer.setTitle("Composer");
            composer.setScene(composer_scene);
            composer.initModality(Modality.NONE);
            composer.initOwner(root_stage);
            double localX = 2 * root_stage.getWidth() / 5;
            double localY = root_stage.getHeight() / 5;
            int size = ua.getChannel().sizeComposerControllers();
            composer.setX(localX + 10 * size);
            composer.setY(localY + 10 * size);
            composer.show();
            composer.setOnCloseRequest(new EventHandler<WindowEvent>() {
              @Override
              public void handle(WindowEvent windowEvent) {
                ua.getChannel().removeComposerController(composer_controller);
                composer_controller.close();
              }
            });
          }
        });
      }catch (IOException e){
        e.printStackTrace();
        Platform.exit();
      }
    }
  }

  @FXML
  private void readButtonAction(ActionEvent event) {
    System.out.println("[*] READ");

    ObservableList<Mail> rows = this.table.getSelectionModel().getSelectedItems();
    this.ua.toggleReadStatus(this.current_folder, rows, true);

    this.table.refresh();
    if(rows.size() > 0){
      this.toggleRead(rows.get(0));
    }else{
      System.err.println("[Controller] readButtonAction: rows.size == 0");
    }
  }

  @FXML
  private void unreadButtonAction(ActionEvent event) {
    System.out.println("[*] UNREAD");

    ObservableList<Mail> rows = this.table.getSelectionModel().getSelectedItems();
    this.ua.toggleReadStatus(this.current_folder, rows, false);

    this.table.refresh();
    if(rows.size() > 0){
      this.toggleRead(rows.get(0));
    }else{
      System.err.println("[Controller] unreadButtonAction: rows.size == 0");
    }
  }

  @FXML
  private void spamButtonAction(ActionEvent event) {
    System.out.println("[*] SPAM");

    this.ua.move(this.table.getSelectionModel().getSelectedItems(), this.current_folder, "spam");
  }

  @FXML
  private void moveButtonAction(ActionEvent event) {
    System.out.println("[*] MOVE");
    String selected_folder = ((MenuItem) event.getSource()).getText().toLowerCase();
    System.out.println(selected_folder);

    this.ua.move(this.table.getSelectionModel().getSelectedItems(), this.current_folder, selected_folder);
  }

  @FXML
  private void replyButtonAction(ActionEvent event) {
    System.out.println("[*] REPLY");
    Mail mail = this.table.getSelectionModel().getSelectedItem();
    String to = current_folder.equals("sent") ? mail.getTo() : mail.getFrom();
    Mail correctMail = new Mail(null, to,  new Date().getTime(), "[RE]: " + mail.getSubject(), "", "", false, false);
    composerWindow(correctMail);
  }

  @FXML
  private void deleteButtonAction(ActionEvent event) {
    System.out.println("[*] DELETE");

    this.ua.delete(this.table.getSelectionModel().getSelectedItems(), this.current_folder);
  }

  @FXML
  private void emptyButtonAction(ActionEvent event) {
    System.out.println("[*] EMPTY");

    this.ua.emptyTrash();
    this.table.getItems().clear();
  }

  @FXML
  private void recoverButtonAction(ActionEvent event) {
    System.out.println("[*] RECOVER");

    this.ua.recover(this.table.getSelectionModel().getSelectedItems());
  }

  private void setTreeView() {
    Label account = new Label(ua.getAddr().get());
    TreeItem<String> mailbox = new TreeItem<String>("", account);

    for (String item : this.folders_arr) {
      boolean is_valid = true;
      ImageView img_icon = null;

      Image img = new Image("file:res/" + item.toLowerCase() + ".png");
      img_icon = new ImageView(img);
      img_icon.setFitHeight(20);
      img_icon.setFitWidth(20);
      img_icon.setPreserveRatio(true);

      mailbox.getChildren().add(new TreeItem<String>(item, img_icon));
    }

    account.setId("account");
    mailbox.setExpanded(true);
    this.folders.setRoot(mailbox);
  }

  private void setToolbar() {
    this.refresh_btn.setGraphic(new ImageView(new Image("file:res/refresh.png")));
    this.refresh_btn.setTooltip(new Tooltip("Refresh"));
    this.compose_btn.setGraphic(new ImageView(new Image("file:res/edit.png")));
    this.compose_btn.setTooltip(new Tooltip("Compose"));
    this.read_btn.setGraphic(new ImageView(new Image("file:res/read.png")));
    this.read_btn.setTooltip(new Tooltip("Mark as read"));
    this.unread_btn.setGraphic(new ImageView(new Image("file:res/unread.png")));
    this.unread_btn.setTooltip(new Tooltip("Mark as unread"));
    this.spam_btn.setGraphic(new ImageView(new Image("file:res/alert.png")));
    this.spam_btn.setTooltip(new Tooltip("Mark as spam"));
    this.delete_btn.setGraphic(new ImageView(new Image("file:res/delete.png")));
    this.delete_btn.setTooltip(new Tooltip("Remove"));
    this.empty_btn.setGraphic(new ImageView(new Image("file:res/fire.png")));
    this.empty_btn.setTooltip(new Tooltip("Empty trash"));
    this.recover_btn.setGraphic(new ImageView(new Image("file:res/recover.png")));
    this.recover_btn.setTooltip(new Tooltip("Recovery"));
    this.recompose_btn.setGraphic(new ImageView(new Image("file:res/edit.png")));
    this.recompose_btn.setTooltip(new Tooltip("Edit"));
    this.reply_btn.setGraphic(new ImageView(new Image("file:res/reply.png")));
    this.reply_btn.setTooltip(new Tooltip("Reply"));
    this.replyall_btn.setGraphic(new ImageView(new Image("file:res/reply_all.png")));
    this.replyall_btn.setTooltip(new Tooltip("Reply all"));
    this.forward_btn.setGraphic(new ImageView(new Image("file:res/forward.png")));
    this.forward_btn.setTooltip(new Tooltip("Forward"));
  }

  private void setStatusBar(String str) {
    this.connectionStatus.setGraphic(new ImageView(new Image("file:res/" + str + ".png")));
    this.connectionStatus.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    this.connectionStatus.setTooltip(new Tooltip("You're " + str + "!"));
    boolean bool = str.equals("disconnected");
    progressBar.setVisible(bool);
    connectingLabel.setVisible(bool);
  }

  private void showMails(String folder) {
    this.body_area.setText("");
    this.table.getItems().clear();
    this.ua.getMails(folder);
  }

  private void toggleToolbar(Mail mail, boolean is_disabled) {
    if(is_disabled){
      this.read_btn.setDisable(true);
      this.unread_btn.setDisable(true);
      this.delete_btn.setDisable(true);
    }else{
      toggleRead(mail);
      toggleStar(mail);
      toggleTrash();
    }

    this.spam_btn.setDisable(is_disabled);
    this.menu_move_to.setDisable(is_disabled);
  }

  private void toggleRead(Mail mail){
    boolean readBtnBool = mail != null && mail.isRead();
    this.read_btn.setDisable(readBtnBool);
    this.unread_btn.setDisable(!readBtnBool);
  }
  private void toggleStar(Mail mail){
    boolean starBtnBool = mail != null && mail.isStarred();
  }
  private void toggleTrash(){
    boolean trashBtnBool = current_folder.toLowerCase().equals("trash");
    this.delete_btn.setDisable(trashBtnBool);
    this.recover_btn.setDisable(!trashBtnBool);
  }

  @FXML
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    this.setTreeView();
    this.setToolbar();
    this.setStatusBar("disconnected");

    this.date_col.setComparator(new Comparator<String>() {

      @Override
      public int compare(String str_1, String str_2) {
        try {
          SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/YYYY");
          Date date_1 = date_format.parse(str_1);
          Date date_2 = date_format.parse(str_2);

          return Long.compare(date_1.getTime(), date_2.getTime());
        }
        catch (ParseException ex) {
          System.err.println("[*] Error during dates parsing!");

          return -1;
        }
      }
    });

    this.subject_col.setCellValueFactory(new PropertyValueFactory<Mail, String>("subject"));
    this.from_col.setCellValueFactory(new PropertyValueFactory<Mail, String>("from"));
    this.date_col.setCellValueFactory(new PropertyValueFactory<Mail, String>("date"));

    this.table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    // Listener TreeView
    this.folders.getSelectionModel().selectedItemProperty()
      .addListener((obs, prev, curr) -> {

        this.showMail.setVisible(false);

        if (curr != null) {

          if(current_folder != null){
            System.out.println("[Controller] current_folder: " + current_folder);
          }

          // Folder in cui mi trovo
          String rightFolder = parseFolderName(curr.getValue().toLowerCase());
          this.current_folder = rightFolder;
          this.ua.setCurrentFolder(rightFolder);

          if (curr.getGraphic().getId() != null && curr.getGraphic().getId().equals("account")) {
            this.table.getItems().clear();
            this.body_area.setText("");
            this.table.setDisable(true);
            this.table.setPlaceholder(new Label("Mailbox related to " + ua.getAddr()));
            this.refresh_btn.setDisable(true);
          } else {
            this.table.setDisable(false);
            this.refresh_btn.setDisable(false);
            this.showMails(this.current_folder);
            this.table.setPlaceholder(new Label("Loading"));

            // Se se in drafts oppure in sent, mostri la colonna To invece della colonna From
            if (this.current_folder.equals("drafts") || this.current_folder.equals("sent")) {
                this.from_col.setText("To");
                this.from_col.setCellValueFactory(new PropertyValueFactory<Mail, String>("to"));
            } else {
                this.from_col.setText("From");
                this.from_col.setCellValueFactory(new PropertyValueFactory<Mail, String>("from"));
            }

            boolean currFolderTrash = this.current_folder.equals("trash");
            this.empty_btn.setVisible(currFolderTrash);
            this.recover_btn.setVisible(currFolderTrash);
            this.recover_btn.setDisable(currFolderTrash);

            this.menu_move_to.getItems().removeAll(this.menu_move_to.getItems());
            for(String folder : folders_arr){
                // Implementa il "move to"
                if(current_folder != null && !current_folder.equals(folder.toLowerCase())){
                    MenuItem mi = new MenuItem(folder);
                    mi.setOnAction(a -> {
                        System.out.println("[Controller] move mails in another folder: " + mi.getText());
                        this.ua.move(this.table.getSelectionModel().getSelectedItems(), this.current_folder, mi.getText().toLowerCase());
                    });
                    this.menu_move_to.getItems().add(mi);
                }
            }//end for
          }
        }//end if curr != null
      });

    // Listener TableView
    this.table.getSelectionModel().selectedItemProperty()
      .addListener((obs, prev, curr) -> {
        if (curr != null) {
          ArrayList<Mail> c = new ArrayList<>();
          c.add(curr);
          ua.toggleReadStatus(current_folder, c, true);

          this.showMail.setVisible(true);

          this.from_area.setText(curr.getFrom());
          this.to_area.setText(curr.getTo());
          this.cc_area.setText((curr.getCc() != null ? curr.getCc() : ""));
          this.date_area.setText(curr.getStrDate());
          this.subject_area.setText(curr.getSubject());
          this.body_area.setText(curr.getBody());

          boolean currFolderDrafts = this.current_folder.equals("drafts");
          this.recompose_btn.setDisable(!currFolderDrafts);
          this.recompose_btn.setVisible(currFolderDrafts);
          this.reply_btn.setDisable(currFolderDrafts);
          this.reply_btn.setVisible(!currFolderDrafts);
          this.replyall_btn.setDisable(currFolderDrafts);
          this.replyall_btn.setVisible(!currFolderDrafts);
          this.forward_btn.setDisable(currFolderDrafts);
          this.forward_btn.setVisible(!currFolderDrafts);

          this.toggleToolbar(this.table.getSelectionModel().getSelectedItem(),false);
        } else {
          this.showMail.setVisible(false);
          this.toggleToolbar(null,true);
        }
      });

    this.table.setRowFactory(row -> new TableRow<Mail>() {
      @Override
      protected void updateItem(Mail mail, boolean empty) {
        super.updateItem(mail, empty);

        if (mail != null && !mail.isRead()) {
           this.setStyle("-fx-font-weight: bold;");
        }
        else {
          this.setStyle("");
        }
      }
    });

    this.folders.getSelectionModel().select(1);
    this.date_col.setSortType(TableColumn.SortType.DESCENDING);
    this.table.getSortOrder().add(this.subject_col);
    this.table.sort();
  }


  // Parsifica il nome della folder
  private String parseFolderName(String str){
    String out = "";
    for(int i = 0; i < str.length(); i++){
      char c = str.charAt(i);
      if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')){
        out += c;
      }
    }
    return out;
  }
}
