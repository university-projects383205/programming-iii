package client;

import common.*;
import javafx.beans.Observable;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.*;

public class UserAgentModel {
  private final ObservableList<Mail> mails =
          FXCollections.observableArrayList(mail -> new Observable[] {
                  mail.subjectProperty(), mail.fromProperty(), mail.dateProperty()
          });  // Mail che mostro nella tableview

  private final ObservableList<CounterMails> counterNewMail =
        FXCollections.observableArrayList(cm -> new Observable[]{
            cm.getIntegerProperty()
        });  // Contantore delle notifiche (uno per ogni folder)

  // ID dell'ultima Operation.GET (per evitare di "visualizzare" il risultato di uno spam di Operation.GET)
  private int lastGetId;

  // Numero delle mail nell'attuale folder
  private IntegerProperty numMails;

  // Folder corrente
  private String currentFolder;

  // Manager client socket
  public ChannelClient channel;

  public String [] folders;

  // ID delle operazioni
  private IDGenerator idg;

  private BooleanProperty logout;

  public UserAgentModel(ChannelClient c){
    channel = c;
    // Creo le cartelle
    folders = new String[] {"Inbox", "Sent", "Drafts", "Spam", "Trash"};
    for(int i = 0; i < folders.length; i++){
      // Imposto i contatori associati alle varie cartelle
      counterNewMail.add(new CounterMails(folders[i]));
    }
    numMails = new SimpleIntegerProperty();
    init();
  }

  public ChannelClient getChannel(){
    return channel;
  }

  public String[] getFolders(){
    String [] cl = new String[folders.length];
    for(int i = 0; i < folders.length; i++)
      cl[i] = folders[i];
    return cl;
  }

  public void logout(){
    System.out.println("[Model]: logout");
    mails.removeAll();
    init();
  }

  private void init(){
    lastGetId = 0;
    numMails.set(-1);
    currentFolder = null;
    for(CounterMails cm : counterNewMail){
      cm.reset();
    }

    if(logout != null){
      logout.set(!logout.get());
    }else{
      logout = new SimpleBooleanProperty(true);
    }

    idg = new IDGenerator();
  }

  public void newMail(String folder){
    System.out.println("[Model] Arrivata nuova mail in " + folder);
    boolean bool = false;
    for(int i = 0; !bool && i < counterNewMail.size(); i++){
      CounterMails cm = counterNewMail.get(i);
      bool = cm.getFolder().equals(folder);
      // Per questioni di sicurezza (magari un utente malintenzionato passa una stringa differente da quelle che noi ci aspettiamo e che consideriamo valide)
      if(bool){
        cm.incr();
      }
      System.out.println("\t" + cm);
    }
  }

  public ObservableList<CounterMails> getCounterNewMail(){
    return counterNewMail;
  }

  public void close(){
    channel.close();
  }

  public BooleanProperty getLogout(){
    return logout;
  }

  public StringProperty getAddr(){
    return channel.getMyAddress();
  }

  public void loadMails(int localId, ArrayList<Mail> list){
    System.out.println("[Model] Arrived request mails with id " + localId + " on " + lastGetId);
    if(localId == lastGetId) {
      boolean bool = false;
      for(int i = 0; !bool && i < counterNewMail.size(); i++){
        CounterMails cm = counterNewMail.get(i);
        bool = cm.getFolder().toLowerCase().equals(currentFolder);
        if(bool){
          // System.out.println("[Model] reset counter " + currentFolder);
          cm.reset(); // Non ho più nuove mail da notificare
        }
      }

      // Per gestire la visualizzazione del messaggio di loading
      this.mails.addAll(list);
      if(list == null){
        System.err.println("[Model] da una get è arrivato un array nullo");
        numMails.set(0);
      }else {
        numMails.set(list.size());
      }
    }
  }

  public void setCurrentFolder(String folder){
    this.currentFolder = folder;
  }

  public String getCurrentFolder() {
      return currentFolder;
  }

  public IntegerProperty getNumMails(){
      return numMails;
  }


  public ObservableList<Mail> getMails() {
      return this.mails;
  }

  public void getMails(String folder) {
    numMails.set(-1);
    String addr = channel.getMyAddress().get();
    if(addr != null && !addr.equals("")){
      Operation op = new Operation(idg.newID(), Operation.GET); // Richiedo una GET delle mail nella folder specificata
      op.setMails(null, folder, null);
      lastGetId = op.getId();

      channel.send(op);
    }
  }

  public void star(Collection<Mail> toStar, String currentFolder){
      for(Mail mail : toStar){
          mail.setIsStarred(true);
      }
      Operation op = new Operation(idg.newID(), Operation.STAR);
      op.setMails(toStar, currentFolder, null);
      channel.send(op);
  }

  public void unstar(Collection<Mail> toUnstar, String currentFolder){
      for(Mail mail : toUnstar){
          mail.setIsStarred(false);
      }
      Operation op = new Operation(idg.newID(), Operation.UNSTAR);
      op.setMails(toUnstar, currentFolder, null);
      channel.send(op);

      //delete the mails from the table if the user is in the starred folder
      if(currentFolder.toLowerCase().equals("starred")){
        int numRemoved = toUnstar.size();
        this.mails.removeAll(toUnstar);
        numMails.set(numMails.get() - numRemoved);
      }
  }

  public void delete(Collection<Mail> toDelete, String currentFolder) {
    Operation op = new Operation(idg.newID(), Operation.DELETE);
    op.setMails(toDelete, currentFolder, null);
    channel.send(op);

    // Delete the mails from the table
    int numRemoved = toDelete.size();
    this.mails.removeAll(toDelete);
    numMails.set(numMails.get() - numRemoved);  // Serve per aggiornare il numero di mail nella folder
  }

  public void recover(Collection<Mail> toRecover) {
    Operation op = new Operation(idg.newID(), Operation.RECOVER);
    op.setMails(toRecover, null, null);
    channel.send(op);

    // Delete the mails from the table (folder trash)
    int numRemoved = toRecover.size();
    this.mails.removeAll(toRecover);
    numMails.set(numMails.get() - numRemoved);
  }

  public void move(Collection<Mail> to_move, String old_folder, String new_folder) {
    Operation op = new Operation(idg.newID(), Operation.MOVE);
    op.setMails(to_move, old_folder, new_folder);
    channel.send(op);

    //delete the mails from the table
    int numRemoved = to_move.size();
    this.mails.removeAll(to_move);
    numMails.set(numMails.get() - numRemoved);
  }

  public void toggleReadStatus(String folder, Collection<Mail> to_toggle, boolean is_read) {
    for(Mail mail : to_toggle)
        mail.setIsRead(is_read);
    String status = Operation.READ;
    if(!is_read)
        status = Operation.UNREAD;
    Operation op = new Operation(idg.newID(), status);
    op.setMails(to_toggle, folder, null);
    channel.send(op);
  }

  public void emptyTrash() {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      alert.setTitle("Empty trash");
      alert.setHeaderText("Are you sure you want to permanently delete all the mails inside the Trash folder?");
      Optional<ButtonType> option = alert.showAndWait();

      if (option.get() == ButtonType.OK) {
          System.out.println("[*] CONFIRMED");
          Operation op = new Operation(idg.newID(), Operation.EMPTY_TRASH);
          op.setMails(null, null, null);
          channel.send(op);

          //remove permanently the mails
        this.mails.removeAll();
        numMails.set(0);
      } else if (option.get() == ButtonType.CANCEL) {
          System.out.println("[*] CANCELLED");
      } else {
          System.out.println("[*] NONE");
      }
  }


  public void saveMail(Mail toSave, String currentFolder){
    Operation op = new Operation(idg.newID(), Operation.SAVE);
    op.setMail(toSave, null, null);
    channel.send(op);
  }

  public void sendMail(Mail toSend, int id){
    Operation op = new Operation(idg.newID(), Operation.SENT);
    op.setSentMail(toSend, id);
    channel.send(op);
  }

  // Genera gli ID delle Operation
  public static class IDGenerator {
    private int currentID;

    public IDGenerator(){
      currentID = 0;
    }
    public int newID(){
      int n = currentID;
      currentID++;
      return n;
    }
  }
}
