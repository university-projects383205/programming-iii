package client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

  // Creo la GUI
  @Override
  public void start(Stage primaryStage) throws Exception{

    ChannelClient channel = new ChannelClient();

    FXMLLoader loader = new FXMLLoader(getClass().getResource("useragent.fxml"));
    UserAgentModel ua = new UserAgentModel(channel);
    channel.setModel(ua);
    UserAgentController ua_controller = new UserAgentController(ua, primaryStage);

    loader.setController(ua_controller);
    primaryStage.getIcons().add(new Image("file:res/client-logo.png"));
    Parent root = loader.load();
    primaryStage.setTitle("Mail Client");
    Scene scene = new Scene(root);
    scene.getStylesheets().add(getClass().getResource("useragent.css").toExternalForm());
    primaryStage.setScene(scene);

    primaryStage.show();
    ua_controller.login();
  }

  public static void main(String[] args) {
    launch(args);
  }
}
