package client;

import common.*;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.SocketException;

public class ReceiverClient extends Thread {
  private ChannelClient parent;
  private ObjectInputStream in;

  public ReceiverClient(ChannelClient cc, ObjectInputStream in){
      parent = cc;
      this.in = in;
  }

  @Override
  public void run() {
    try {
      while(true) {
        // Attende oggetti
        Object obj = in.readObject();
        if (obj instanceof Operation) {
          Operation op = (Operation) obj;
          System.out.println("[ReceiverClient]op ricevuta: " + op);
          switch (op.getOperation()) {
            case Operation.GET:
            /*
            case Operation.DELETE:
            case Operation.EMPTY_TRASH:
            case Operation.MOVE:
            case Operation.READ:
            case Operation.RECOVER:
            case Operation.SAVE:
            case Operation.SENT:
            case Operation.STAR:
            case Operation.UNREAD:
            case Operation.UNSTAR:
            */
              parent.loadMails(op);
              break;
            case Operation.LOGIN:
              parent.responseLogin(op);
              break;
            case Operation.NOTIFY:
              parent.newMail(op);
              break;
            case Operation.GENERAL_ERROR:
              // Non dovrebbe capitare (a meno di bug)
              parent.errorMsg(op);
              break;
            case Operation.COMPOSER_MSG:
              parent.composerMsg(op);
              break;
            default:
              System.out.println("[ReceiverClient] operazione non considerata: " + op.getOperation());
              break;
          }
        }else{
          System.out.println("[ReceiverClient] tipo non riconosciuto: " + obj.getClass());
        }
      }
    } catch(NullPointerException err){
      System.err.println("[ReceiverClient]: " + err.getMessage());
    } catch(EOFException | SocketException e) {
      System.err.println("[ReceiverClient]: Disconnected (" + e.getMessage() + ")");
    }catch (ClassNotFoundException | IOException ex) {
      ex.printStackTrace();
      System.exit(1);
    }finally {
      parent.disconnected();
    }
  }
}
