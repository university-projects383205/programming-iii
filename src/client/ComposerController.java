package client;

import java.util.regex.Pattern;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import common.*;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class ComposerController implements Initializable {
  private int id; // ID del composer
  private UserAgentModel ua;
  private UserAgentController uaController;
  private Mail mail;

  @FXML
  private Button send_btn;

  @FXML
  private Button save_btn;

  @FXML
  private TextField to_txt;

  @FXML
  private TextField cc_txt;

  @FXML
  private TextField subject_txt;

  @FXML
  private TextArea body_txt;

  @FXML
  private Label msg;

  /*
  * [0]: field to_txt   (necessary)
  * [1]: field cc_txt   (unnecessary)
  **/
  private boolean [] validMail; // Verifica sulla validità di To e CC


  public ComposerController(int id, UserAgentModel ua, UserAgentController uaController, Mail mail) {
    this.id = id;
    this.ua = ua;
    this.uaController = uaController;
    this.mail = mail;

    validMail = new boolean[2];
    validMail[0] = false;
    validMail[1] = true;
  }

  public int getId(){
    return id;
  }

  public void close(){
    //chiudere la finestra
    Stage stage = (Stage) save_btn.getScene().getWindow();
    stage.close();
  }


  @FXML
  private void sendButtonAction(ActionEvent event){
    System.out.println("[*] Send email");
    Mail toSend = new Mail(ua.getAddr().get(), to_txt.getText(),
            new Date().getTime(), subject_txt.getText(),
            body_txt.getText(), cc_txt.getText(),
            false, false);
    this.ua.sendMail(toSend, id);
  }



  @FXML
  private void saveButtonAction(ActionEvent event){
    System.out.println("[*] Saved email");

    Mail toSave = new Mail(ua.getAddr().get(), to_txt.getText(),
                            new Date().getTime(), subject_txt.getText(),
                            body_txt.getText(), cc_txt.getText(),
                            false, false);
    this.ua.saveMail(toSave, uaController.getCurrent_folder());
    uaController.tableRefresh();
  }


  // Quando abilitare il tasto di invio
  private void setSendButton(){
    boolean bool = true;
    for(int i = 0; bool && i < validMail.length; i++){
      bool = bool && validMail[i];
    }
    send_btn.setDisable(!bool);
    uaController.tableRefresh();
  }

  private void setToolbar() {
    try {
      this.send_btn.setGraphic(new ImageView(new Image(new FileInputStream("res/sent.png"))));
      this.save_btn.setGraphic(new ImageView(new Image(new FileInputStream("res/drafts.png"))));
    }
    catch (IOException ex) {
      System.err.println("[*] Error during toolbar setup!");
    }
  }

  // Match sulla regexp per gli indirizzi mail
  private void setPattern(TextInputControl tic, String regex, int index){
    tic.textProperty().addListener((obs,old,niu)->{
      boolean bool = Pattern.matches(regex, tic.getText());
      if(bool){
        tic.setStyle("-fx-border-color: inherit");
      }else{
        tic.setStyle("-fx-border-color: red");
      }

      if(index >= 0 && index < validMail.length){
        validMail[index] = bool;
      }
      setSendButton();
    });
  }

  public void setMsg(String localMsg){
    Platform.runLater(new Runnable() {
      @Override
      public void run() {
        if(localMsg.equals(Operation.FLAG_SUCCESS)){
          close();
        }else{
          msg.setText(localMsg);
        }
      }
    });
  }


  @FXML
  @Override
  public void initialize(URL url, ResourceBundle resourceBundle) {
    this.setToolbar();

    String emailPattern = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}";

    setPattern(to_txt, emailPattern, 0);
/*
    setPattern(subject_txt, ".{1, }", -1, "min 1, max 255 characters");
    setPattern(body_txt, "", .{1, }, "min 1 max 5000 characters");
    setPattern(cc_txt,  emailPattern + "(\\s" + emailPattern + ")*", -1, "empty or example@ex.it example1@ex.it ...");
 */
    setPattern(cc_txt,  "(" + emailPattern + "(\\s" + emailPattern + ")*){0,1}", 1);

    if(mail != null){
        if(mail.getTo() != null)
            to_txt.setText(mail.getTo());
        if(mail.getCc() != null)
            cc_txt.setText(mail.getCc());
        if(mail.getSubject() != null)
            subject_txt.setText(mail.getSubject());
        if(mail.getBody() != null)
            body_txt.setText(mail.getBody());
    }
  }
}

