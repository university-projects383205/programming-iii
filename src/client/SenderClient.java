package client;

// 30L

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.SocketException;

public class SenderClient extends Thread{
  private ChannelClient parent;
  private ObjectOutputStream out;
  private Object obj; // Oggetto da inviare (Operation)

  public SenderClient(ChannelClient c, ObjectOutputStream out, Object obj){
    this.parent = c;
    this.out = out;
    this.obj = obj;
  }

  @Override
  public void run() {
    try {
      // Invia l'operation
      out.writeObject(obj);
    } catch(NullPointerException ex){
      System.err.println("[SenderClient] " + ex.getMessage());
    } catch(EOFException | SocketException e) {
      System.err.println("[SenderClient]: Server is offline (" + e.getMessage() + ")");
      parent.logout();
      parent.disconnected();
    } catch (IOException ex) {
      ex.printStackTrace();
      parent.disconnected();
      System.exit(1);
    }
  }
}
