package common;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import java.io.Serializable;
import java.text.SimpleDateFormat;

public class Mail implements Serializable {
  private static final long serialVersionUID = 1L; // Utilizzato per serializzare/deserializzare correttamente le mail

  private String from;
  private String to;
  private long date;
  private String subject;
  private String body;

  private int id;
  private boolean is_starred;
  private boolean is_read;
  private String cc;
  private String prev_folder;

  public Mail(String from, String to, long date, String subject, String body, String cc, boolean is_read, boolean is_starred) {
    this.id = -1;
    this.subject = subject;
    this.from = from;
    this.to = to;
    this.date = date;
    this.body = body;
    this.cc = cc;
    this.is_read = is_read;
    this.is_starred = is_starred;
  }

  public Mail(Mail clone){
    this.id = -1;
    this.subject = new String(clone.getSubject());
    this.from = new String(clone.getFrom());
    this.to = new String(clone.getTo());
    this.date = clone.getDate();
    this.body = new String(clone.getBody());
    this.cc = new String(clone.getCc());
    this.is_read = false;
    this.is_starred = false;
  }

  private String cloneStr(String str){
    String out = "";
    for(int i = 0; i < str.length(); i++){

    }
    return out;
  }

  public int getId() {
    return this.id;
  }

  public String getFrom() {
    return this.from;
  }

  public String getTo() {
    return this.to;
  }

  public long getDate() {
    return this.date;
  }

  public String getSubject() {
    return this.subject;
  }

  public String getBody() {
    return this.body;
  }

  public String getCc() {
    return this.cc;
  }

  public boolean isRead() {
    return this.is_read;
  }

  public boolean isStarred() {
    return this.is_starred;
  }

  public String getPrevFolder() {
    return this.prev_folder;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public void setDate(long date) {
    this.date = date;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public void setCc(String cc) {
    this.cc = cc;
  }

  public void setIsRead(boolean is_read) {
    this.is_read = is_read;
  }

  public void setIsStarred(boolean is_starred) {
    this.is_starred = is_starred;
  }

  public void setPrevFolder(String prev_folder) {
    this.prev_folder = prev_folder;
  }

  public StringProperty fromProperty() {
    return new SimpleStringProperty(this.from);
  }

  public StringProperty toProperty() {
    return new SimpleStringProperty(this.to);
  }

  public StringProperty dateProperty() {
    return new SimpleStringProperty(this.getStrDate());
  }

  public StringProperty subjectProperty() {
    return new SimpleStringProperty(this.subject);
  }

  public StringProperty bodyProperty() {
    return new SimpleStringProperty(this.body);
  }

  public BooleanProperty isReadProperty() {
    return new SimpleBooleanProperty(this.is_read);
  }

  public String getStrDate() {
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    return formatter.format(date);
  }

  @Override
  public boolean equals(Object obj) {
    boolean output = obj != null && obj.getClass() == this.getClass();

    if (output) {
      Mail to_compare = (Mail) obj;

      output = this.getId() == to_compare.getId();
    }

    return output;
  }

  @Override
  public String toString() {
    return String.valueOf(this.id);
  }

}
