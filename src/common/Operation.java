package common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

public class Operation implements Serializable {
  private static final long serialVersionUID = 30L;

  public static final String GET = "get";
  public static final String SENT = "sent";
  public static final String SAVE = "save";
  public static final String MOVE = "move";
  public static final String STAR = "star";
  public static final String UNSTAR = "unstar";
  public static final String READ = "read";
  public static final String UNREAD = "unread";
  public static final String DELETE = "delete";
  public static final String RECOVER = "recover";
  public static final String EMPTY_TRASH = "emptyTrash";
  public static final String LOGIN = "login";
  public static final String NOTIFY = "notify";
  public static final String GENERAL_ERROR = "gen_err";
  public static final String COMPOSER_MSG = "composer_msg";

  public static final String FLAG_SUCCESS = "Successfull";

  private int id;
  private String operation;
  private Object[] array;

  public Operation(int id, String operation){
    this.id = id;
    this.operation = operation;
  }

  /*
   * array[0]: (int) length arrayList<Mail>
   * array[1]: (Mail) first mail
   * array[.]: (Mail)
   * array[length]: (Mail) last mail
   * if(fstFolder != null) array[length + 1]: currentFolder
   * if(sndFolder != null) array[length + 2]: newFolder
   * */
  public void setMails(Collection<Mail> coll, String fstFolder, String sndFolder){
    if(standardOperation(operation)){
      boolean fst = fstFolder != null;
      boolean snd = sndFolder != null;
      int mailsLength = (coll != null ? coll.size() : 0);
      int length = 1 + mailsLength + (fst ? 1 : 0) + (snd ? 1 : 0);
      array = new Object[length];
      array[0] = mailsLength;
      for(int i = 0; i < mailsLength; i++){
        array[i + 1] = ((List<Mail>)coll).get(i);
      }
      if(fst){
        array[mailsLength + 1] = fstFolder;
        if(snd){
          array[mailsLength + 2] = sndFolder;
        }
      }
    }else{
      setException();
    }
  }

  public static boolean standardOperation(String op){
    boolean output = false;
    switch (op){
      case Operation.GET:
      case Operation.DELETE:
      case Operation.EMPTY_TRASH:
      case Operation.MOVE:
      case Operation.READ:
      case Operation.RECOVER:
      case Operation.SAVE:
      // case Operation.SENT:
      case Operation.STAR:
      case Operation.UNREAD:
      case Operation.UNSTAR:
        output = true;
        break;
    }
    return output;
  }

  // Uguale a quello di prima ma con una sola mails
  public void setMail(Mail one, String fstFolder, String sndFolder){
    if(standardOperation(operation)){
      boolean fst = fstFolder != null;
      boolean snd = sndFolder != null;
      int mailsLength = one != null ? 1 : 0;
      int length = 1 + mailsLength + (fst ? 1 : 0) + (snd ? 1 : 0);
      array = new Object[length];
      array[0] = mailsLength;
      if(mailsLength > 0){
        array[1] = one;
      }
      if(fst){
        array[mailsLength + 1] = fstFolder;
        if(snd){
          array[mailsLength + 2] = sndFolder;
        }
      }
    }else{
      setException();
    }
  }

  // Restituisce le mail soltanto se l'operazione lo consente (ad esempio la notify non ritorna una lista di mail)
  public ArrayList<Mail> getMails() {
    ArrayList<Mail> list = null;
    if(standardOperation(operation)) {
      int length = (Integer) array[0];
      if(length >= array.length){
        System.err.println("[Operation " + id + "](" + operation + "): Attribute length is invalid");
      }
      list = new ArrayList<Mail>(length);
      for(int i = 1; i < length + 1; i++){
        Mail tmp = (Mail)array[i];
        list.add(tmp);
      }
      return list;
    }else{
      getException();
    }
    return list;
  }

  // Esegue getMails ma ritornando un vector che è thread-safe
  public Vector<Mail> getMailsAsVector() {
    Vector<Mail> list = null;
    if(standardOperation(operation)) {
      int length = (Integer) array[0];
      if(length >= array.length){
        System.err.println("[Operation " + id + "](" + operation + "): Attribute length is invalid");
      }
      list = new Vector<Mail>(length);
      for(int i = 1; i < length + 1; i++){
        Mail tmp = (Mail)array[i];
        list.add(tmp);
      }
      return list;
    }else{
      getException();
    }
    return list;
  }

  // Restituisce la folder soltanto se l'operazione lo consente
  public String getCurrentFolder() {
    String curr = null;
    if(standardOperation(operation)){
      int length = (Integer)array[0];
      if(length + 2 <= array.length){
        curr = (String)array[length + 1];
      }
    }else{
      getException();
    }
    return curr;
  }

  // Restituisce il folder di destinazione solo se l'operazione lo consente
  public String getNewFolder() {
    String nie = null;
    if(standardOperation(operation)){
      int length = (Integer)array[0];
      if(length + 3 <= array.length){
        nie = (String)array[length + 2];
      }
    }else{
      getException();
    }
    return nie;
  }


  /*
   * array[0]: myAddress
   * array[1]: login msg
   */
  public void setLogin(String addr, String msg){
    if(operation.equals(Operation.LOGIN)){
      array = new Object[2];
      array[0] = addr;
      array[1] = msg;
    }else{
      setException();
    }
  }

  // Restituisce l'indirizzo se l'operazione lo consente
  public String getMyAddress(){
    String addr = null;
    if(operation.equals(Operation.LOGIN)){
      addr = (String) array[0];
    }else{
      getException();
    }
    return addr;
  }

  // Restituisce il login msg se l'operazione lo consente
  public String getLoginMsg(){
    String msg = null;
    if(operation.equals(Operation.LOGIN)){
      msg = (String) array[1];
    }else{
      getException();
    }
    return msg;
  }

  /*
   * array[0]: folder
   * in folder there are new mail
   * */
  public void setNotify(String folder){
    if(operation.equals(Operation.NOTIFY)){
      array = new Object[1];
      array[0] = folder;
    }else{
      setException();
    }
  }

  // Restituisce il messaggio di notifica se l'operazione lo consente (i.e. il folder che ha ricevuto una notifica)
  public String getNotify(){
    String msg = null;
    if(operation.equals(Operation.NOTIFY) ||
      operation.equals(Operation.GENERAL_ERROR)){
      msg = (String) array[0];
    }else{
      getException();
    }
    return msg;
  }

  /*
   * array[0]: id del composer mittente
   * array[1]: messaggio di ritorno --> successfull e/o lista utenti non registrati presenti nel cc
   */
  public void setComposerMsg(int id, String msg){
    if(operation.equals(Operation.COMPOSER_MSG)){
      array = new Object[2];
      array[0] = id;
      array[1] = msg;
    }else{
      setException();
    }
  }

  // Restituisce il msg del composer se l'operation lo consente
  public String getComposerMsg(){
    String msg = null;
    if(operation.equals(Operation.COMPOSER_MSG)){
      msg = (String)array[1];
    }else{
      getException();
    }
    return msg;
  }

  // Restituisce l'id del composer
  public int getComposerID(){
    int id = -1;
    if(operation.equals(Operation.COMPOSER_MSG)){
      id = ((Integer) array[0]).intValue();
    }else{
      getException();
    }
    return id;
  }

  /*
   * array[0]: id del composer
   * array[1]: mail inviata
   */
  public void setSentMail(Mail mail, int id){
    if(operation.equals(Operation.SENT)){
      array = new Object[2];
      array[0] = id;
      array[1] = mail;
    }else{
      setException();
    }
  }

  public Mail getSentMail(){
    Mail mail = null;
    if(operation.equals(Operation.SENT)){
      mail = (Mail)array[1];
    }else{
      getException();
    }
    return mail;
  }

  public int getSentID(){
    int id = -1;
    if(operation.equals(Operation.SENT)){
      id = ((Integer)array[0]).intValue();
    }else{
      getException();
    }
    return id;
  }

  // UNUSED
  public void setErrorMsg(String errMsg){
    if(operation.equals(Operation.GENERAL_ERROR)){
      array = new Object[1];
      array[0] = errMsg;
    }else{
      setException();
    }
  }


  public int getId() {
    return id;
  }

  public String getOperation() {
    return operation;
  }

  public void setOperation(String str){
    operation = str;
  }

  @Override
  public String toString() {
    String out = "id: " + id + ", operarion: " + operation;
    int length = -100;
    switch (operation){
      case Operation.GET:
        length = (Integer) array[0];
        out += ", n.Mails: " + length;
        out += ", currentFolder: " + array[length + 1];
        break;
      case Operation.DELETE:
        length = (Integer) array[0];
        out += ", n.Mails: " + length;
        out += ", currentFolder: " + array[length + 1];
        break;
      case Operation.EMPTY_TRASH:
        break;
      case Operation.MOVE:
        length = (Integer) array[0];
        out += ", n.Mails: " + length;
        out += ", currentFolder: " + array[length + 1];
        out += ", newFolder: " + array[length + 2];
        break;
      case Operation.READ:
        length = (Integer) array[0];
        out += ", n.Mails: " + length;
        out += ", currentFolder: " + array[length + 1];
        break;
      case Operation.RECOVER:
        out += ", n.Mails: " + array[0];
        break;
      case Operation.SAVE:
        out += ", n.Mails: " + array[0];
        break;
      // case Operation.SENT:
      case Operation.STAR:
        length = (Integer) array[0];
        out += ", n.Mails: " + length;
        out += ", currentFolder: " + array[length + 1];
        break;
      case Operation.UNREAD:
        length = (Integer) array[0];
        out += ", n.Mails: " + length;
        out += ", currentFolder: " + array[length + 1];
        break;
      case Operation.UNSTAR:
        length = (Integer) array[0];
        out += ", n.Mails: " + length;
        out += ", currentFolder: " + array[length + 1];
        break;
      case Operation.SENT:
        Mail mail = (Mail)getSentMail();
        out += ", from Mail: " + mail.getFrom() + ", to Mail: " + mail.getTo() + ", id composer: " + getSentID();
        break;
      case Operation.LOGIN:
        out += ", myAddress: " + array[0] + ", loginMsg: " + array[1];
        break;
      case Operation.NOTIFY:
        out += ", folder: " + array[0];
        break;
      case Operation.GENERAL_ERROR:
        out += ", work in progress";
        break;
      case Operation.COMPOSER_MSG:
        out += ", composer id: " + array[0] + ", msg: " + array[1];
        break;
      default:
        out += ", (undefine)array.length: " + (array != null ? array.length : -1);
        break;
    }
    return out;
  }

  private void setException(){
    System.err.println("[Operation " + id + "]: Impossibile set this attributes because the operation is " + operation);
  }

  private void getException(){
    System.err.println("[Operation " + id + "]: There aren't attributes for " + operation + " operation");
  }

}

