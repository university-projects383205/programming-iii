package server;

import common.*;
import java.util.Iterator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.Vector;

public class Folder implements Iterable<Mail> {
  private User user;
  private String name;
  private Vector<Mail> mails;

  public Folder(User user, String name) {
    this.user = user;
    this.name = name.toLowerCase();
    this.mails = new Vector<Mail>();
    this.generateBinaryFile();
  }

  public String getName() {
    return this.name;
  }

  // Genera i file binari nel caso in cui non esistano
  private void generateBinaryFile() {
    File file = new File("users/" + this.user.getUsername() + "/" + this.name + ".ser");

    if (!file.exists()) {
      FileOutputStream fos = null;
      ObjectOutputStream oos = null;

      try {
        fos = new FileOutputStream(file);
        oos = new ObjectOutputStream(fos);
      }
      catch (IOException ex) {
        // ...
      }
      finally {
        try {
          if (fos != null) {
            fos.close();
          }

          if (oos != null) {
            oos.close();
          }
        }
        catch (IOException ex) {
          // ...
        }
      }
    }
  }

  // Legge le mail da file binario
  private void deserializeMails() {
    ObjectInputStream reader = null;

    try {
      reader = new ObjectInputStream(new FileInputStream("users/" + this.user.getUsername() + "/" + this.name + ".ser"));

      try {
        Object o = reader.readObject();

        if (!this.name.equals("starred")) {
          if (o instanceof Vector) {
            this.mails = (Vector<Mail>) o;

            for (Mail mail : this.mails) {
              this.user.addId(mail.getId());
            }
          }
        }
        else {
          if (o instanceof Vector) {
            Vector<StarredMail> starred_mails = (Vector<StarredMail>) o;

            for (StarredMail mail : starred_mails) {
              this.user.addId(mail.getId());
            }
          }
        }
      }
      catch (EOFException ex) {
        System.out.println("[*] Reached EOF!");
      }
      catch (ClassNotFoundException ex) {
        ex.printStackTrace();
      }

      reader.close();
      reader = null;
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
    finally {
      try {
        if (reader != null) {
          reader.close();
        }
      }
      catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }

  // Legge le mail già salvate
  public Vector<Mail> getMails() {
    if (!this.name.equals("starred")) {
      this.deserializeMails();

      return this.mails;
    }
    else {
      return null;
    }
  }

  // Scrive le mail su file binario
  private void serializeMails(Vector<Mail> mails) {
    ObjectOutputStream writer = null;

    try {
      writer = new ObjectOutputStream(new FileOutputStream("users/" + this.user.getUsername() + "/" + this.name + ".ser"));

      if (!this.name.equals("starred")) {
        writer.writeObject(mails);
      }
      else {
        Vector<StarredMail> starred_mails = this.toStarred(mails);

        for (StarredMail mail : starred_mails) {
          writer.writeObject(mail);
        }
      }
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
    finally {
      try {
        if (writer != null) {
          writer.close();
        }
      }
      catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }

  public void updateMails(Vector<Mail> mails) {
    serializeMails(mails);
  }

  private Vector<StarredMail> toStarred(Vector<Mail> starred_mails) {
    Vector<StarredMail> converted_mails = new Vector<StarredMail>();

    for (Mail mail : starred_mails) {
      int id = mail.getId();
      // String folder = mail.getCurrFolder();
      String folder = this.name;

      converted_mails.add(new StarredMail(id, folder));
    }

    return converted_mails;
  }

  @Override
  public Iterator<Mail> iterator() {
    return this.getMails().iterator();
  }

  @Override
  public boolean equals(Object o) {
    if (this.getClass().equals(o.getClass())) {
      Folder to_compare = (Folder) o;

      return this.getName().equals(to_compare.getName());
    }
    else {
      return false;
    }
  }
}
