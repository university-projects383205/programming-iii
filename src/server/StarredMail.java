package server;

public class StarredMail {
  private int id;
  private String folder;

  public StarredMail(int id, String folder) {
    this.id = id;
    this.folder = folder;
  }

  public int getId() {
    return this.id;
  }

  public String getFolder() {
    return this.folder;
  }
}
