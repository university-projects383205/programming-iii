package server;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainServer extends Thread{
  final String MAIN_DIR_LOCATION = "users/";
  public static final String ANSI_BLUE = "\u001B[34m";
  public static final String ANSI_RESET = "\u001B[0m";
  private ServerSocket mainServer;
  private ExecutorService pool;
  private AllUsers users;
  private Model model;

  public MainServer() {
    users = new AllUsers();
    File maindirectory = new File(MAIN_DIR_LOCATION);
    String[] usersList = maindirectory.list();

    // assert usersList != null;
    if (usersList != null) {
      System.out.println(ANSI_BLUE + "[Server]: Utenti registrati" + ANSI_RESET);
      for (int i = 0; i < usersList.length; i++) {
        File tmp = new File(maindirectory.getAbsolutePath(), usersList[i]);
        users.add(new UserStatus(tmp.getName()));
        System.out.println(ANSI_BLUE + "\t" + tmp.getName() + ANSI_RESET);
      }
    }
    else {
      System.err.println("Questo non doveva accadere...");
      System.exit(2);
    }
  }

  public void setModel(Model m) {
    this.model = m;
  }

  // Creo il serversocket e la pool di thread inizializzata al numero di core disponibili
  public void startServer(int port) {
    try {
      mainServer = new ServerSocket();
      mainServer.setReuseAddress(true);
      mainServer.bind(new InetSocketAddress(port));
      this.pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    } catch (IOException e){
      e.printStackTrace();
      System.out.println("[Server]: Errore caricamento server!!" + ANSI_RESET);
      model.writeLogs("Creation_fail","Errore caricamento del server!",Log.ERROR_STR, "Server");

    }
  }

  // Stoppo il server, scollego tutto gli account e termino la pool di thread generata in precedenza
  public void stopServer() {
    try {
      if (this.mainServer != null) {
        this.users.shutdownAll();
        this.pool.shutdown();
        this.mainServer.close();
      }
    }
    catch (IOException ex) {
      ex.printStackTrace();
      System.out.println("[Server]: Errore spegnimento server!!" + ANSI_RESET);
      model.writeLogs("Shutdown_fail","Errore spegnimento del server!",Log.ERROR_STR, "Server");
    }
  }

  // Thread principale del server di posta
  @Override
  public void run() {
    // Ho cancellato super.run() --> R.I.P. super.run()
    System.out.println(ANSI_BLUE +"[Server]: MainServer Online" + ANSI_RESET);
    model.writeLogs("Online", "Il Main Server è online",Log.INFO_STR, "Server");
    try {
      while (!this.mainServer.isClosed()) {
        System.out.println(ANSI_BLUE + "[Server]: MainServer Ready" + ANSI_RESET);
        model.writeLogs("Ready", "Il Main Server è pronto", Log.INFO_STR, "Server");
        try {
            Socket socket = mainServer.accept();
            pool.execute(new IstanceServer(socket, users, model));
        } catch (IOException e) {
          System.out.println(ANSI_BLUE + "[Server]: Client connection error!!" + ANSI_RESET);
          if (!this.mainServer.isClosed()) {
            model.writeLogs("Connection_error", "Il Client non è riuscito a connettersi al server", Log.ERROR_STR, "Server");
          }
        }
        System.out.println(ANSI_BLUE + "[Server]: Connection..." + ANSI_RESET);
        model.writeLogs("Connection...", "Il Main Server si sta connettendo", Log.INFO_STR, "Server");
      }
    } catch (Exception e) {
      e.printStackTrace();
      model.writeLogs("Unhandled_exception", e.getMessage(), Log.ERROR_STR, "E poi il server");
    } finally {
      stopServer();
    }
  }
}
