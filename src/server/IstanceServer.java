package server;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;
import common.*;

public class IstanceServer extends Thread {
  public static final String ANSI_CYAN = "\u001B[36m";
  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_RED = "\u001B[31m";
  private Socket socket;
  private ObjectInputStream in;
  private ObjectOutputStream out;
  private User user;
  private UserModel userModel;
  private long start; // Timestamp dell'ultima operazione fatta dal client
  private AllUsers users;
  private Model model;
  private boolean server_offline; // Segnala se il server è offline

  public IstanceServer(Socket s, AllUsers users, Model m) {
    setDaemon(true);
    model = m;
    socket = s;
    this.users = users;
    this.server_offline = false;
    try {
      in = new ObjectInputStream(socket.getInputStream());
      out = new ObjectOutputStream(socket.getOutputStream());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void close() {
    this.server_offline = true;
  }

  @Override
  public void run() {

    // Configurazione del timer
    start = System.currentTimeMillis();
    Timer timer = new Timer(true);
    TimerTask task = new MyTask();
    timer.schedule(task, 0, 5000);
    try {
      if (server_offline) {
        timer.cancel();
        timer.purge();
      }

      // Controllo esistenza email
      System.out.println(this + ": Socket OK");
      model.writeLogs("Socket_OK", "La connessione col client è stata stabilita", Log.INFO_STR, whoAmI());
      if (!connect()) {
        throw new Exception("NOTEMAIL");
      }
      System.out.println(this + ": Connection Established");
      model.writeLogs("Connection_Established", "LogIn avvenuto con successo", Log.INFO_STR, whoAmI());
      System.out.println(this + ": Automatic Folders Update Allowed");
      model.writeLogs("Automatic_Folders_Update_Allowed", "Aggiornamento automatico dei folders attivato", Log.INFO_STR, whoAmI());


      // Ciclo delle operazioni
      while (!socket.isClosed()) {
        System.out.println(this + "Request Acepted");
        start = System.currentTimeMillis();
        Object obj = in.readObject();
        System.out.println(ANSI_CYAN + this + "[Operation]: " + obj + ANSI_RESET);
        model.writeLogs("Request_Acepted", obj.toString(), Log.INFO_STR , whoAmI());

        if (!(obj instanceof Operation))
          throw new Exception("NOTOP");

        // Eseguo le operazioni corrette
        Operation op = (Operation) obj;

        switch (op.getOperation()) {
            case Operation.GET: {
              // Serializzi le mail e poi le mostri
              String currFolder = op.getCurrentFolder();
              userModel.updateFolder(currFolder);
              Vector<Mail> arr = userModel.getMails(currFolder);
              op.setMails(arr, currFolder, null);
              out.writeObject(op);
              break;
            }
            case Operation.DELETE: {
              String currFolder = op.getCurrentFolder();
              userModel.move(op.getMailsAsVector(), currFolder, "trash");
              break;
            }
            case Operation.EMPTY_TRASH: {
              userModel.emptyTrash();
              break;
            }
            case Operation.MOVE: {
              String currFolder = op.getCurrentFolder();
              String destFolder = op.getNewFolder();
              userModel.move(op.getMailsAsVector(), currFolder, destFolder);
              break;
            }
            case Operation.READ: {
              userModel.setIsRead(op.getMailsAsVector(), op.getCurrentFolder(), true);
              break;
            }
            case Operation.UNREAD: {
              userModel.setIsRead(op.getMailsAsVector(), op.getCurrentFolder(), false);
              break;
            }
            case Operation.RECOVER: {
              // Recupera le mail dal cestino
              Vector<Mail> arr = op.getMailsAsVector();
              Vector<Mail> tmp = new Vector<Mail>();
              for (Mail toRecover : arr) {
                tmp.add(toRecover);
                userModel.move(tmp, "trash", toRecover.getPrevFolder());
                tmp.clear();
              }
              break;
            }
            case Operation.SAVE: {
              Object syncr = users.getSyncr(user);
              userModel.add(syncr, op.getMails().get(0), "drafts");
              notifyNewMail("Drafts");
              break;
            }
            case Operation.SENT:{
              Mail mail = op.getSentMail();
              Set<String> allIns = new HashSet<>(); // Tutte i destinartari (sia validi che invalidi)
              Set<String> onlyDest = new HashSet<>(); // Tutti quelli che hanno un indirizzo valido
              String to = mail.getTo();
              allIns.add(to);
              if(users.isRegistered(to)){
                onlyDest.add(to);
              }

              String cc = mail.getCc();
              if(cc != null && !cc.equals("")){
                String[] ccs = cc.split(" ");
                for(int i = 0; i < ccs.length; i++) {
                  allIns.add(ccs[i]);
                  if (users.isRegistered(ccs[i])) {
                    onlyDest.add(ccs[i]);
                  }
                }
              }

              // newDest e newCC sono il destinatario e il CC della mail che è stata inviata (solo per chi esegue la send)
              String newDest = null;
              String newCC = null;
              for(String dest : onlyDest){
                sendMail(dest, mail, onlyDest);
                if(newDest == null){
                  newDest = dest;
                }else{
                  if(newCC == null){
                    newCC = dest;
                  }else{
                    newCC += " " + dest;
                  }
                }
              }

              // msg è la differenza insiemistica tra allIns e onlyDest (i.e. indirizzi non validi)
              String msg = null;
              for(String all : allIns) {
                if(!onlyDest.contains(all)){
                  if(msg == null){
                    msg = all;
                  }else {
                    msg += ", " + all;
                  }
                }
              }

              // Se è stato inviato ad almeno una persona, inserisci la mail in "sent" (i.e. la send è andata a buon fine per almeno un indirizzo)
              if(onlyDest.size() > 0){
                Object syncr = users.getSyncr(user);
                mail.setTo(newDest);
                mail.setCc(newCC);
                userModel.add(syncr, mail, "sent");

                if(msg == null){
                  msg = Operation.FLAG_SUCCESS;
                }else{
                  msg += " not registered&" + Operation.FLAG_SUCCESS;
                }
              }

              int id = op.getSentID();
              op.setOperation(Operation.COMPOSER_MSG);
              op.setComposerMsg(id, msg);
              out.writeObject(op);
              System.out.println(this + ": Inviata ricevuta di ritorno op: " + op.getOperation() + ", id: " + op.getId() + ", msg: " + msg);
              model.writeLogs("Return_Message", "Inviata ricevuta di ritorno op: " + op.getOperation() + ", id: " + op.getId() + ", msg: " + msg, Log.INFO_STR, whoAmI());
              break;
            }
            case Operation.STAR: {
              System.out.println("[WIP] Star operation");
              break;
            }
            case Operation.UNSTAR: {
              System.out.println("[WIP] Unstar operation");
              break;
            }
            default: {
              System.err.println(this + ": Operation not recognized " + op.getOperation());
              model.writeLogs("Operation_fail", "Operazione non riconosciuta", Log.WARNING_STR, whoAmI());
              break;
            }
          }//end switch
      }//end while
    } catch (SocketException etime) {
      // Se il messaggio è "socket closed", è caduta la connessione con il server (il timer ha chiuso il socket)
      if (!etime.getMessage().equals("Socket closed"))
        etime.printStackTrace();
    } catch (EOFException eof) {
      // Quando il client chiude la connesisone, chiudo anche gli stream associati
      System.out.println(ANSI_RED + this + ": Connection Close" + ANSI_RESET);
      model.writeLogs("Connection_close", "Chiusura della connessione", Log.WARNING_STR, whoAmI());
    } catch (ClassNotFoundException | IOException ex) {
      // Probabilmente UNREACHABLE
      model.writeLogs("Unhandled_exception", ex.getMessage(), Log.ERROR_STR, whoAmI());
      ex.printStackTrace();
    } catch (Exception e) {
      // Inserita una mail non valida
      if (e.getMessage().equals("NOTEMAIL")) {
        System.out.println(ANSI_RED + this + ": Email is not valid" + ANSI_RESET);
        model.writeLogs("Email_not_valid", "Email non valida", Log.ERROR_STR, whoAmI());
      }
      // Inserita un'operation non valida / una classe differente da Operation
      else if (e.getMessage().equals("NOTOP")) {
        System.out.println(ANSI_RED + this + ": Operation not valid" + ANSI_RESET);
        model.writeLogs("Operation_fail", "Operazione non riconosciuta", Log.ERROR_STR, whoAmI());
      }
      else {
        e.printStackTrace();
        model.writeLogs("Unhandled_exception", e.getMessage(), Log.ERROR_STR, whoAmI());
      }
    } finally {
      try {
        /*
        // L'utente viene messo offline
        users.offline(user);
        */

        // Chiudo il socket e fermo il timer
        socket.close();
        timer.cancel();
        if(users.isOnline(user)) {
          userModel.updateFolders();
          // System.out.println(this + ": Folders Updated");
          // L'utente viene messo offline
          users.offline(user);
          model.writeLogs("Mails_Merges", "Mails salvate nel file in chiusura", Log.INFO_STR, whoAmI());
        }
      } catch (IOException | NullPointerException e) {
        System.err.println("Unhandled exception");
        model.writeLogs("Unhandled_exception", e.getMessage(), Log.ERROR_STR, whoAmI());
      }
    }
    System.out.println(ANSI_RED + this + ": Connection Lost" + ANSI_RESET);
    model.writeLogs("Connection_Lost", "La connessione con il client è stata persa", Log.WARNING_STR, whoAmI());
  }

  private void sendMail(String dest, Mail mail, Set<String> allDest) throws IOException {
    // Clono la mail e imposto il destinatario
    Mail destMail = new Mail(mail);
    destMail.setTo(dest);

    // Ho la lista dei destinatari validi (scrivo il CC corretto ma escludo la mia mail)
    String ccMail = null;
    for(String localCC : allDest){
      if(!localCC.equals(dest)){
        if(ccMail == null){
          ccMail = localCC;
        }else{
          ccMail += " " + localCC;
        }
      }
    }
    destMail.setCc(ccMail);

    System.out.println(this + ": invio da " + user.getUsername() + " a " + dest);
    UserModel destUserModel = null;
    // Se è online, faccio la get della sua model, altrimenti ne genero una io
    if(users.isOnline(dest)){
      destUserModel = users.getModel(dest);
    }else {
      destUserModel = new UserModel(new User(dest));
    }
    // Ottengo l'oggetto di sincronizzazione
    Object destSyncr = users.getSyncr(user);

    // Se l'utente è nella blacklist, le sue mail andranno direttamente nello spam e non nella inbox
    String dst_folder = destUserModel.getUser().isBlacklisted(user.getUsername()) ? "spam" : "inbox";

    destUserModel.add(destSyncr, destMail, dst_folder);
    destUserModel.updateFolder(dst_folder);
    System.out.println(this + ": mail da " + user.getUsername() + " a " + dest + " inviata");

    // Notifico l'utente solo se non è nello spam
    if (dst_folder.equals("inbox")) {
      users.notifyUserOnline(dest, "Inbox");
    }
  }


  private class MyTask extends TimerTask {
    public void run() {
      try {
        // Ogni 5 secondi aggiorna le cartelle (se l'utente è online)
        if (users.isOnline(user)) {
          userModel.updateFolders();
          // System.out.println(this + ": Folders Updated");
        }

        // Dopo 5 minuti (o se il server è offline) chiudo la connessione
        if (server_offline || System.currentTimeMillis() - start > 300000) {
          socket.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  public boolean connect() throws IOException, ClassNotFoundException {
    Object obj = in.readObject();
    boolean result = false;

    // Se il login è avvenuto con successo, creo l'oggetto User e aggiungo l'utente alla lista di utenti online
    if (obj instanceof Operation) {
      Operation op = (Operation) obj;
      String name = op.getMyAddress();
      String msg = isUser(name);
      result = msg.equals(Operation.FLAG_SUCCESS);

      if (result) {
        System.out.println(this + ": Welcome " + name);
        System.out.println(ANSI_CYAN + this + "[Operation]: " + obj + ANSI_RESET);

        user = new User(name);
        userModel = new UserModel(user);

        users.online(user, this);
      }
      op.setLogin(name, msg);
      out.writeObject(op);
    }
    return result;
  }


  // Verifico che l'utente esista
  private String isUser(String usr) {
    String msg = Operation.FLAG_SUCCESS;

    if(users.isRegistered(usr)) {
      if(users.isOnline(usr)) {
        msg = "User " + usr + " è già online";
      }

    }else{
      msg = "User " + usr + " non è registrato";
    }
    return msg;
  }

  // Notifica la presenza di una nuova mail
  public void notifyNewMail(String folder) throws IOException{
    Operation op = new Operation(-1, Operation.NOTIFY);
    op.setNotify(folder);
    out.writeObject(op);
  }

  protected UserModel getUserModel(){
    return userModel;
  }

  @Override
  public String toString() {
    return "\t[SubServer]["+ whoAmI()+"]";
  }

  // Ritorna il chiamante
  public String whoAmI() {
    return (user != null) ? user.getUsername() : "SubServer";
  }
}
