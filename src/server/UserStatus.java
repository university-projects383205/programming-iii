package server;

// Status del singolo utente (sia online che offline)
public class UserStatus {
  private String user;
  private IstanceServer connection;
  private Object obj;

  public UserStatus(String user){
    this.user = user;
    connection = null;
    obj = new Object();
  }

  public void shutdown() {
    if (connection != null) {
      connection.close();
    }
  }

  public String getUser() {
    return user;
  }
  public IstanceServer getConnection(){
    return connection;
  }
  public boolean getOnline(){
    return connection != null;
  }

  public void setConnection(IstanceServer connection) {
    this.connection = connection;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public Object getSyncr(){
    return obj;
  }
}
