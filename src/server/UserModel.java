package server;

import common.Mail;

import java.util.*;

public class UserModel {
  private User user;
  private Map<String, Folder> folders;
  private Map<String, Vector<Mail>> map;

  public UserModel(User user) {
    this.user = user;
    this.folders = this.user.getFolders();
    this.map = new HashMap<String, Vector<Mail>>();

    for (String folder : this.folders.keySet()) {
      if (!folder.equals("starred")) {
        this.map.put(folder, this.folders.get(folder).getMails());
      }
      else {
        this.map.put(folder, this.fromStarred(this.user.getStarredMails()));
      }
    }
    this.user.resetCounter();
  }

  public User getUser() {
    return this.user;
  }

  private Vector<Mail> fromStarred(Vector<StarredMail> starred_mails) {
    Vector<Mail> converted_mails = new Vector<Mail>();

    for (StarredMail mail : starred_mails) {
      int id = mail.getId();
      Vector<Mail> folder_mails = this.map.get(mail.getFolder());
      boolean found = false;

      for (int i = 0; i < folder_mails.size() && !found; i++) {
        if (folder_mails.get(i).getId() == id) {
          converted_mails.add(folder_mails.get(i));
          found = true;
        }
      }
    }

    return converted_mails;
  }

  public void updateFolders() {
    for (String folder : folders.keySet()) {
      this.updateFolder(folder);
    }
  }

  public void updateFolder(String folder) {
    this.folders.get(folder).updateMails(this.map.get(folder));
  }

  public Vector<Mail> getMails(String folder) {
    return this.folders.get(folder).getMails();
  }

  public void move(Vector<Mail> to_move, String src_folder, String dst_folder) {
    for (Mail mail : to_move) {

      if (this.map.get(src_folder).contains(mail)) {
        if (src_folder.equals("spam")) {
          this.user.removeFromBlacklist(mail.getFrom());
        }

        if (dst_folder.equals("trash")) {
          mail.setPrevFolder(src_folder);
        }
        else if (dst_folder.equals("spam")) {
          this.user.addToBlacklist(mail.getFrom());
        }

        this.map.get(dst_folder).add(mail);
        this.map.get(src_folder).remove(mail);
      }
    }
  }

  public void add(Object syncr, Mail to_add, String folder) {
    synchronized (syncr){
      to_add.setId(this.user.getCounter());
      this.user.updateCounter();
      this.map.get(folder).add(to_add);
    }
  }

  public void setIsRead(Vector<Mail> to_toggle, String folder, boolean to_read) {
    for (Mail mail : to_toggle) {
      int index = this.map.get(folder).indexOf(mail);

      if (index >= 0) {
        this.map.get(folder).get(index).setIsRead(to_read);
      }
    }
  }

  public void emptyTrash() {
    this.map.get("trash").clear();
  }
}
