package server;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.text.SimpleDateFormat;
import java.util.Base64;

public class Log {

  private StringProperty log;
  public static final String CONNECTION = "connection";
  public static final String DISCONNECTION = "disconnection";
  public static final String MAIL_SENT = "mail send";
  public static final String RECEIVED_MAIL = "received mail";
  public static final String NOT_SENT = "mail not sent";


  private StringProperty type;
  public static final String INFO_STR = "Info";
  public static final String WARNING_STR = "Warning";
  public static final String ERROR_STR = "Error";

  private LongProperty date;
  private String msg;
  private StringProperty source;

  public Log(){
    init();
  }

  private void init(){
    type = new SimpleStringProperty();
    log = new SimpleStringProperty();
    date = new SimpleLongProperty();
    source = new SimpleStringProperty();
  }

  /*
   * type + ";" + date + ";" + log + ";" + msg + ";" + source + "\n";
   * [0]: type
   * [1]: date
   * [2]: log
   * [3]: msg
   * [4]: source
   */
  public Log(String str, boolean toDecode){
    init();
    if(toDecode){
      toRead(str);
    }else {
      String[] log = str.split(";");

      setType(log[0]);
      setDate(log[1]);
      setLog(log[2]);
      setMsg(log[3]);
      setSource(log[4]);
    }
  }

  public Log(String type, Long date, String log, String msg, String source){
    init();

    setType(type);
    setDate(date);
    setLog(log);
    setMsg(msg);
    setSource(source);
  }

  //TYPE
  public StringProperty getTypeProperty() {
    return type;
  }
  public String getType(){
    return type.get();
  }
  public void setType(String type){
    this.type.set(type);
  }

  //LOG
  public StringProperty getLogProperty() {
    return this.log;
  }
  public void setLog(String log) {
    this.log = new SimpleStringProperty(log);
  }
  public String getLog(){
    return this.log.get();
  }

  //DATE
  public StringProperty getDateProperty() {
    return new SimpleStringProperty(this.getStrDate());
  }
  public String getStrDate(){
    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
    return formatter.format(getDate().longValue());
  }
  public void setDate(long date) {
    this.date.set(date);
  }
  public void setDate(String date){
    long conv = Long.parseLong(date);
    setDate(conv);
  }
  public Long getDate(){
    return this.date.get();
  }

  //SOURCE
  public StringProperty getSourceProperty() {
    return this.source;
  }
  public String getSource(){
    return this.source.get();
  }
  public void setSource(String source) {
    this.source.set(source);
  }

  //MSG
  public String getMsg(){
    return msg;
  }
  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String toWrite(){
    String msg = Base64.getEncoder().encodeToString(this.msg.getBytes());
    return getType() + ";" + getDate() + ";" + getLog() + ";" + msg + ";" + getSource() + "\n";
  }

  // Decode message
  public void toRead(String str){
    String[] log = str.split(";");
    setType(log[0]);
    setDate(log[1]);
    setLog(log[2]);
    byte [] bytes = Base64.getDecoder().decode(log[3]);
    setMsg(new String(bytes));
    setSource(log[4]);
  }
}
