package server;

import java.util.Iterator;

public class FolderIterator implements Iterator<Folder> {
  private User user;
  private Folder current_folder;

  public FolderIterator(User user) {
    this.user = user;
    this.current_folder = user.getFolder("inbox");
  }

  @Override
  public boolean hasNext() {
    return !this.current_folder.getName().equals("trash");
  }

  @Override
  public Folder next() {
    Folder next_folder = null;

    switch (this.current_folder.getName()) {
      case "inbox":
        next_folder = this.user.getFolder("sent");
        break;

      case "sent":
        next_folder = this.user.getFolder("starred");
        break;

      case "starred":
        next_folder = this.user.getFolder("drafts");
        break;

      case "drafts":
        next_folder = this.user.getFolder("spam");
        break;

      case "spam":
        next_folder = this.user.getFolder("trash");
        break;

      default:
        break;
    }

    return next_folder;
  }
}
