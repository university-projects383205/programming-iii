package server;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Model {
  public static final int ONE_DAY = 86400000;
  private final String DIRECTORY_LOGS = "opt/";

  private int numFileLog;
  private boolean online;

  private List<Log> allLogs;
  private ObservableList<Log> selectedLogs = FXCollections.observableList(new Vector<Log>());

  private boolean infoSelected;
  private boolean warningsSelected;
  private boolean errorsSelected;

  private Vector<String> logFilter;
  private Vector<String> dateFilter;
  private Vector<String> sourceFilter;

  private MainServer mainServer;
  private Lock rl;
  private Lock wl;

  private Lock rlObservable;
  private Lock wlObservable;

  private int lines;


  public Model(){
    ReadWriteLock rwl = new ReentrantReadWriteLock();
    this.rl = rwl.readLock();
    this.wl = rwl.writeLock();
    ReadWriteLock rwlObservable = new ReentrantReadWriteLock();
    this.rlObservable = rwlObservable.readLock();
    this.wlObservable = rwlObservable.writeLock();

    allLogs = new ArrayList<>();

    numFileLog = 0;
    // Conto quanti log ci sono già
    File d = new File(DIRECTORY_LOGS);
    if(!d.exists())
      new File("opt").mkdir();

    String [] a = d.list();
    for (int i = 0;i < a.length; i++) {
      if(a[i].length() > 3){
        String str = a[i].substring(0, 3);
        if(str.equals("log"))
          numFileLog++;
      }
    }
  }

  // Imposto le checkbox
  public void setTypesNoUpdate(boolean infoStatus, boolean warningsStatus, boolean errorsStatus){
    infoSelected = infoStatus;
    warningsSelected = warningsStatus;
    errorsSelected = errorsStatus;
  }

  public ObservableList<Log> getLogs(){
    return selectedLogs;
  }

  // Accende il server spawnando un thread MainServer
  public void serverOn(){
    this.mainServer = new MainServer();
    this.mainServer.startServer(4862);
    this.mainServer.setModel(this);
    this.mainServer.start();

    online = true;
    allLogs = new ArrayList<>();
    checkFileLog();
    readLogs();
  }

  // Spegne il server
  public void serverOff(){
    this.mainServer.stopServer();
    online = false;
  }

  public void readLogs(){
    BufferedReader br = null;

    try {
      br = new BufferedReader(new FileReader(pathLogFile()));
      String line = br.readLine();

      while (line != null) {
        wl.lock();
        allLogs.add(new Log(line, true));
        wl.unlock();
        lines++;
        line = br.readLine();
      }
    } catch (IOException err) {
      err.printStackTrace();
      System.exit(1);
    } finally {
      try {
        br.close();
      }catch (Exception err){}
    }

    selected();
  }

  private String pathLogFile(){
    return DIRECTORY_LOGS + "log" + numFileLog + ".txt";
  }

  public void writeLogs(String log, String msg, String type, String source){
    checkFileLog();

    System.out.println("A new log has been received");
    long date = new Date().getTime();
    Log tmp = new Log(type, date, log, msg, source);

    BufferedWriter bw = null;
    try {
      bw = new BufferedWriter(new FileWriter(pathLogFile(), true));
      bw.write(tmp.toWrite());
      wl.lock();
      allLogs.add(tmp);
      wl.unlock();

      checkLines();
      lines++;
    } catch (IOException err) {
      err.printStackTrace();
      System.exit(1);
    }finally {
      try {
        bw.close();
      }catch (Exception err1){}
    }

    if(online){
      conditionAdd(tmp);
    }

  }

  // Crea un nuovo log ogni 2000 linee
  private void checkLines(){
    if(lines > 2000){
      lines = 0;
      createNewFile();
    }
  }

  // Verifica se esiste già il file di log e verifica quanto tempo è passato dalla creazione di quest'ultimo
  private synchronized void checkFileLog(){
    File maindirectory = new File(DIRECTORY_LOGS);
    String[] fileLogs = maindirectory.list();
    if(fileLogs.length <= 0){
      System.err.println("file not exists");
      createNewFile();
    }else {
      File f = new File(pathLogFile());
      BasicFileAttributes attr = null;
      try {
        attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
        long fileDate = attr.creationTime().to(TimeUnit.MILLISECONDS);
        if (datedFile(fileDate, new Date().getTime())) {
          createNewFile();
        }
      }catch (IOException ex){
        ex.printStackTrace();
      }

    }
  }

  public static boolean datedFile(long past, long present){
    return present - past >= ONE_DAY;
  }

  // Crea un nuovo file di log
  public void createNewFile() {
    try {
      numFileLog++;
      File file = new File(pathLogFile());
      if (file.createNewFile()) {

        wl.lock();
        allLogs.clear();
        wl.unlock();

        wlObservable.lock();
        selectedLogs.remove(0, selectedLogs.size());
        wlObservable.unlock();

        System.err.println("New file log is been created: " + pathLogFile());
      } else {
        System.err.println("[Error] New file log isn't been created: " + pathLogFile());
      }
    }catch (IOException ex){
      ex.printStackTrace();
      System.exit(1);
    }
  }

  /*
   * Legenda:
   *   tutti null: seleziona tutto
   *   tutti array vuoti: non selezionare nulla
   *   altro: filtra solo gli elementi presenti negli array
  */
  public void setFilters(Vector<String> logF, Vector<String> dateF, Vector<String> sourceF,
                         boolean infoS, boolean warningS, boolean errorS){
    logFilter = logF;
    dateFilter = dateF;
    sourceFilter = sourceF;
    setTypesNoUpdate(infoS, warningS, errorS);

    selected();
  }

  /*
   * Legenda:
   *  Premessa: vengono selezionate le stringhe con il campo type abilitato (le checkbox)
   *  caso filtri vuoti: se tutti i vector *filter sono null, allora vengono inseriti tutti i log idonei alla premessa
   *  caso filtri non vuoti: un log per essere selezionato o non ha vincoli per un dato campo (indicato con il
   *                         vector *filter = null) o deve ritornare true da tutte le invocazioni di isContains
   *                         dove i campi sono vincolati dal filtro
   */
  // Quali log visualizzare e quali no (fa da wrapper per conditionAdd)
  private synchronized void selected(){
    wlObservable.lock();
    selectedLogs.remove(0, selectedLogs.size());
    wlObservable.unlock();

    rl.lock();
    for (int i = 0; i < allLogs.size(); i++) {
      Log line = allLogs.get(i);

      conditionAdd(line);
    }
    rl.unlock();
  }

  private void conditionAdd(Log line){
    if ((infoSelected && line.getType().equals(Log.INFO_STR)) ||
      (warningsSelected && line.getType().equals(Log.WARNING_STR)) ||
      (errorsSelected && line.getType().equals(Log.ERROR_STR))) {
      boolean toAdd = logFilter == null || isContains(line.getLog().toLowerCase(), logFilter);
      toAdd = toAdd && (dateFilter == null || isContains(line.getStrDate().toLowerCase(), dateFilter));
      toAdd = toAdd && (sourceFilter == null || isContains(line.getSource().toLowerCase(), sourceFilter));

      if (toAdd){
        wlObservable.lock();
        selectedLogs.add(line);
        wlObservable.unlock();
      }
    }
  }

  // Controlla che str sia contenuto in arr (feature nuova)
  // WIP: In arrivo con Java 20
  private boolean isContains(String str, Vector<String> arr){
    boolean isContains = false;
    for(int i = 0; !isContains && i < arr.size(); i++)
      isContains = str.equals(arr.get(i));
    return isContains;
  }

  // Set delle varie checkbox
  public void infoSelected(boolean isSelected){
    infoSelected = isSelected;
    selected();
  }

  public void warningsSelected(boolean isSelected){
    warningsSelected = isSelected;
    selected();
  }

  public void errorsSelected(boolean isSelected){
    errorsSelected = isSelected;
    selected();
  }

  // Ricerca nei log
  public boolean [] dataSearchBar(String input){
    boolean [] output = new boolean[3];
    for(int i = 0; i < output.length; i++)
      output[i] = true;

    Vector<String> logFilter = new Vector<>();
    Vector<String> dateFilter = new Vector<>();
    Vector<String> typeFilter = new Vector<>();
    Vector<String> sourceFilter = new Vector<>();
    int fill = 0;       //0: seleziona tutto, 1: non selezionare nulla, 2: filtra

    String standard = input.replace(" ", "").toLowerCase();
    System.out.println("filters: [" + standard + "]");
    String[] filters = standard.split(";");     //split every filter (field: toFilter)
    for(String filter : filters){
      String[] field = filter.split(":");       //split field and toFilter
      switch (field.length){
        case 0: fill = 0; break;      //empty searchbar
        case 1:                       //pattern not complete
          if(field[0].equals("")){
            fill = 0;
          }else{
            fill = 1;
          }
          break;
        case 2:                       //pattern correct
          fill = 2;
          boolean insLog = field[0].equals("log");
          boolean insDate = !insLog && field[0].equals("date");
          boolean insType = !insDate && field[0].equals("type");
          boolean insSource = !insType && field[0].equals("source");
          if(!insLog && !insDate && !insType && !insSource){
            System.out.println("field " + field[0] + " doesn't exist");
            fill = 1;
          }

          if(fill == 2){
            String[] datas = field[1].split(",");
            for(String data : datas){
              if(insLog){
                logFilter.add(data);
              }else if(insDate){
                if(data.length() > 10){
                  String corr = data.substring(0, 10) + " " + data.substring(10);
                  dateFilter.add(corr);
                }else{
                  fill = 1;
                }
              } else if(insType){
                typeFilter.add(data);
              } else if(insSource){
                sourceFilter.add(data);
              }else{
                System.err.println("Impossible case: field not selected => [" + field[0] + "]");
              }
            }//end-for datas
          }
          break;
        default: fill = 1; break;           //pattern not correct
      }
    }//end-for filters

    if(fill == 0){                //seleziona tutto
      setFilters(null, null, null, true, true, true);
    }else if(fill == 1){          //non seleziona nulla
      Vector<String> empty = new Vector<>();
      setFilters(empty, empty, empty, true, true, true);
    }else {
      boolean selInfo = true, selWarnings = true, selErrors = true;
      if(typeFilter.size() > 0){
        selInfo = false; selWarnings = false; selErrors = false;
        for(String str : typeFilter){
          selInfo = selInfo || str.equals("info");
          selWarnings = selWarnings || str.equals("warning");
          selErrors = selErrors || str.equals("error");
        }
      }

      if(logFilter.size() == 0)
        logFilter = null;

      if(dateFilter.size() == 0)
        dateFilter = null;

      if(sourceFilter.size() == 0)
        sourceFilter = null;

      output[0] = selInfo;
      output[1] = selWarnings;
      output[2] = selErrors;

      setFilters(logFilter, dateFilter, sourceFilter, selInfo, selWarnings, selErrors);
    }//end-if fill
    return output;
  }
}
