package server;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class TestServer extends Application{
  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_GREEN = "\033[0;92m";
  @Override
  public void start(Stage primaryStage) throws Exception{
    FXMLLoader console = new FXMLLoader(getClass().getResource("server.fxml"));
    Parent root = console.load();
    Model model = new Model();
    Controller controller = console.getController();
    controller.init(model);

    primaryStage.setTitle("MailBox Server");
    primaryStage.getIcons().add(new Image("file:res/server-logo.png"));
    primaryStage.setOnCloseRequest(event -> {
      System.exit(1);
    });
    Scene scene = new Scene(root, 800, 600);
    primaryStage.setScene(scene);
    primaryStage.show();

  }
  public static void main(String[] args) {
    System.out.println(ANSI_GREEN + "[Main]: OK" + ANSI_RESET);
    launch(args);
  }
}
