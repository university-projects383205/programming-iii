package server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AllUsers {
  private ArrayList<UserStatus> users;
  private Lock rlStatus;
  private Lock wlStatus;


  public AllUsers(){
    users = new ArrayList<>();
    ReadWriteLock rwlStatus = new ReentrantReadWriteLock();
    rlStatus = rwlStatus.readLock();
    wlStatus = rwlStatus.writeLock();
  }

  // Disconnette tutti gli utenti
  public void shutdownAll() {
    for (UserStatus us : users) {
      if (us.getOnline()) {
        us.shutdown();
      }
    }
  }

  // Aggiunge un utente alla lista degli utenti registrati
  protected void add(UserStatus us){
    users.add(us);
  }

  // L'utente è registrato o meno
  public boolean isRegistered(String user){
    boolean registered = false;
    for(int i = 0; !registered && i < users.size(); i++){
      registered = users.get(i).getUser().equals(user);
    }
    return registered;
  }

  // Imposta l'utente come online
  public void online(User user, IstanceServer is){
    if(user != null){
      setOnline(user.getUsername(), is);
    }else{
      System.err.println("[AllUsers][online] user must never be null");
    }
  }

  // Imposta l'utente come offline
  public void offline(User user){
    if(user != null){
      setOnline(user.getUsername(), null);
    }else{
      System.out.println("[AllUsers][offline] user is null");
    }
  }

  private void setOnline(String user, IstanceServer is){
    int i = idxUser(user);
    wlStatus.lock();
    users.get(i).setConnection(is);
    wlStatus.unlock();
  }

  // Ritorna l'id dell'utente
  private int idxUser(String user){
    int i = 0;
    boolean found = false;
    while(!found && i < users.size()){
      found = users.get(i).getUser().equals(user);
      if(!found){
        i++;
      }
    }
    if(!found){
      i = -1;
      System.err.println("[AllUsers][idxUser]: Utente " + user + " non registrato");

    }
    return i;
  }

  // L'utente è online oppure no
  public boolean isOnline(User user){
    boolean online = user != null && isOnline(user.getUsername());
    return online;
  }


  public boolean isOnline(String user){
    int idx = idxUser(user);
    rlStatus.lock();
    boolean online = users.get(idx).getOnline();
    rlStatus.unlock();
    return online;
  }

  // Invia una notifica di nuova mail all'utente specificato se online
  public void notifyUserOnline(String usr, String folder) throws IOException {
    if(isOnline(usr)){
      int i = idxUser(usr);
      IstanceServer connection = users.get(i).getConnection();
      connection.notifyNewMail(folder);
    }
  }

  // Restituisce un lock per l'aggiunta di nuove mail
  public Object getSyncr(User user){
    Object output = null;
    if(user != null){
      int i = idxUser(user.getUsername());
      output = users.get(i).getSyncr();
    }else{
      System.err.println("[AllUsers][online] user must never be null");
    }
    return output;
  }

  // Restituisce il modello associato all'utente specificato
  public UserModel getModel(String usr){
    UserModel userModel = null;
    int i = idxUser(usr);
    IstanceServer is = users.get(i).getConnection();
    if(is != null){
      userModel = is.getUserModel();
    }
    return userModel;
  }

}
