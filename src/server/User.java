package server;

import common.Mail;

import java.util.*;

public class User implements Iterable<Folder> {
  private String username;
  private Map<String, Folder> folders;
  private Vector<StarredMail> starred_mails;
  private Vector<Integer> existing_id;
  private Vector<String> blacklist;
  private int counter;


  public User(String username) {
    this.username = username;

    this.starred_mails = new Vector<StarredMail>();
    this.existing_id = new Vector<Integer>();

    this.folders = new HashMap<String, Folder>();
    this.folders.put("inbox", new Folder(this, "inbox"));
    this.folders.put("sent", new Folder(this, "sent"));
    this.folders.put("drafts", new Folder(this, "drafts"));
    this.folders.put("spam", new Folder(this, "spam"));
    this.folders.put("trash", new Folder(this, "trash"));

    this.counter = 1;

    this.blacklist = new Vector<String>();
    for (Mail mail : this.folders.get("spam").getMails()) {
      this.addToBlacklist(mail.getFrom());
    }
  }

  public void resetCounter() {
    if (!existing_id.isEmpty()) {
      this.counter = Collections.max(existing_id) + 1;
    }
  }

  public String getUsername() {
    return this.username;
  }

  public Map<String, Folder> getFolders() {
    return this.folders;
  }

  public int getCounter() {
    return this.counter;
  }

  public void updateStarredMails(StarredMail starred_mail) {
    this.starred_mails.add(starred_mail);
  }

  public void updateCounter() {
    this.counter++;
  }

  public void addId(int id) {
    this.existing_id.add(id);
  }

  public Folder getFolder(String folder) {
    return this.folders.get(folder.toLowerCase());
  }

  public Vector<StarredMail> getStarredMails() {
    return this.starred_mails;
  }

  public boolean isBlacklisted(String username) {
    return this.blacklist.contains(username);
  }

  public void addToBlacklist(String username) {
    if (!username.equals(this.username)) {
      this.blacklist.add(username);
    }
  }

  public void removeFromBlacklist(String username) {
    this.blacklist.remove(username);
  }

  @Override
  public Iterator<Folder> iterator() {
    return new FolderIterator(this);
  }
}
