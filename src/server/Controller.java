package server;

import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Callback;

public class Controller {
  public static final String ANSI_BLUE = "\u001B[34m";
  public static final String ANSI_RESET = "\u001B[0m";

  @FXML
  private ToggleButton power;
  @FXML
  private Button reboot;

  @FXML
  private CheckBox info;
  @FXML
  private CheckBox warnings;
  @FXML
  private CheckBox errors;

  @FXML
  private TableView tableLog;
  @FXML
  private TableColumn<Log, String> log_col;
  @FXML
  private TableColumn<Log, String> type_col;
  @FXML
  private TableColumn<Log, String> date_col;
  @FXML
  private TableColumn<Log, String> source_col;

  @FXML
  private TextField searchBar;

  @FXML
  private TextArea msg_txt;

  private Label statusServer = new Label("Server offline");
  private Label statusLog = new Label("No logs");

  private Model model;

  private ListProperty<Log> lst;


  // Handler per il tasto di accensione/spegnimento
  @FXML
  private void handlePower(ActionEvent event){
    Platform.runLater(new Runnable() {
        @Override
        public void run() {
          String status = power.isSelected() ? "on" : "off";
          System.out.println(ANSI_BLUE + "[Server]: " + status + ANSI_RESET);
          if(power.isSelected()){
            power.setGraphic(new ImageView(new Image("file:res/power-on.png")));
            model.serverOn();
            tableLog.setPlaceholder(statusLog);
            disableInteractions(false);
          }else{
            power.setGraphic(new ImageView(new Image("file:res/power-off.png")));
            model.serverOff();
            tableLog.getItems().clear();
            tableLog.setPlaceholder(statusServer);
            disableInteractions(true);
          }
        }
      }
    );
  }

  private void disableInteractions(boolean disable){
    this.info.setDisable(disable);
    this.warnings.setDisable(disable);
    this.errors.setDisable(disable);
    this.reboot.setDisable(disable);
    this.searchBar.setDisable(disable);
    this.searchBar.setText("");
    this.msg_txt.setDisable(disable);
    this.msg_txt.setText("");
  }

  // Handler per il tasto di reboot
  @FXML
  private void handleReboot(ActionEvent event){
    System.out.println(ANSI_BLUE + "[Server]: Reboot" + ANSI_RESET);

    power.setGraphic(new ImageView(new Image("file:res/power-off.png")));
    model.serverOff();
    power.setSelected(false);
    tableLog.getItems().clear();
    tableLog.setPlaceholder(statusServer);
    disableInteractions(true);

    power.setGraphic(new ImageView(new Image("file:res/power-on.png")));
    model.serverOn();
    power.setSelected(true);
    tableLog.setPlaceholder(statusLog);
    disableInteractions(false);
  }

  // Handler per le checkbox relative alla visualizzazione dei log
  @FXML
  private void handleInfo(ActionEvent event){
    String status = info.isSelected() ? " " : " not ";
    System.out.println("Info:" + status + "selected");

    model.infoSelected(info.isSelected());
  }

  @FXML
  private void handleWarnings(ActionEvent event){
    String status = warnings.isSelected() ? " " : " not ";
    System.out.println("Warnings:" + status + "selected");

    model.warningsSelected(warnings.isSelected());
  }

  @FXML
  private void handleErrors(ActionEvent event){
    String status = errors.isSelected() ? " " : " not ";
    System.out.println("Errors:" + status + "selected");

    model.errorsSelected(errors.isSelected());
  }

  // Handler per la ricerca dei log
  @FXML
  private void handleSearchBar(ActionEvent event){
    boolean [] val = model.dataSearchBar(searchBar.getText());
    info.setSelected(val[0]);
    warnings.setSelected(val[1]);
    errors.setSelected(val[2]);
  }

  // Colorazione differenti tipi di log
  private void colorRow(TableColumn column){
    column.setCellFactory(new Callback<TableColumn<Log, String>,
      TableCell<Log, String>>()
    {
      @Override
      public TableCell<Log, String> call(
        TableColumn<Log, String> param) {
        return new TableCell<Log, String>() {
          @Override
          protected void updateItem(String item, boolean empty) {
            if (!empty) {
              int currentIndex = indexProperty()
                .getValue() < 0 ? 0 : indexProperty().getValue();
              String typeInTheRow = param.getTableView().getItems().get(currentIndex).getType();

              if (typeInTheRow.equals("Error"))
                setTextFill(Color.RED);
              else if (typeInTheRow.equals("Warning"))
                setTextFill(Color.ORANGE);
              else if (typeInTheRow.equals("Info"))
                setTextFill(Color.BLUE);
              else                        //caso teoricamente impossibile, ma trattato comunque
                setTextFill(Color.PINK);
              setText(item);
            }
          }
        };
      }
    });
  }


  public void init(Model model) {
    this.model = model;
    model.setTypesNoUpdate(info.isSelected(), warnings.isSelected(), errors.isSelected());

    lst = new SimpleListProperty<Log>(model.getLogs());
    lst.addListener((obs, oldSelection, newSelection) -> tableLog.refresh());

    colorRow(log_col);
    colorRow(type_col);
    colorRow(date_col);
    colorRow(source_col);

    this.log_col.setCellValueFactory(cellData -> cellData.getValue().getLogProperty());
    this.type_col.setCellValueFactory(cellData -> cellData.getValue().getTypeProperty());
    this.date_col.setCellValueFactory(cellData -> cellData.getValue().getDateProperty());
    this.source_col.setCellValueFactory(cellData -> cellData.getValue().getSourceProperty());

    // Stampa i messaggi selezionati
    this.tableLog.getSelectionModel().selectedItemProperty().addListener((obs, prev, curr) -> {
      if (curr != null) {
        String msg;
        if(curr.getClass() == Log.class){
          msg = ((Log)curr).getMsg();
        }else{
          msg = "Fatal error";
          System.err.println(curr.getClass() + " not accepted here");
          System.exit(1);
        }
        msg_txt.setText(msg);
      } else {
        msg_txt.setText("");
      }
    });

    this.tableLog.setItems(this.model.getLogs());
    this.tableLog.setPlaceholder(statusServer);
    disableInteractions(true);

    this.reboot.setGraphic(new ImageView(new Image("file:res/reboot.png")));
    this.power.setGraphic(new ImageView(new Image("file:res/power-off.png")));
    this.power.setStyle("-fx-background-radius: 50%");
  }
}
